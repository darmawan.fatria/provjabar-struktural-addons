from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale
from string import upper


class report_pemetaan_struktural_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_pemetaan_struktural_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_detail_pemetaan_jabatan' : self._get_detail_pemetaan_jabatan,
            'get_format_date' : self._get_format_date,
            'to_upper':self._to_upper,
           
        })
    def _get_detail_pemetaan_jabatan(self,data):
        obj_data=self.pool.get('pemetaan.jabatan').browse(self.cr,self.uid,data['data_ids'])
        return obj_data

    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def _to_upper(self,a_string):
        return a_string and upper(a_string) or ''
    

class wrapped_report_pemetaan_struktural_rotasi(osv.AbstractModel):
    _name = 'report.df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_rotasi_report'
    _inherit = 'report.abstract_report'
    _template = 'df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_rotasi_report'
    _wrapped_report_class = report_pemetaan_struktural_report
class wrapped_report_pemetaan_struktural_promosi(osv.AbstractModel):
    _name = 'report.df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_promosi_report'
    _inherit = 'report.abstract_report'
    _template = 'df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_promosi_report'
    _wrapped_report_class = report_pemetaan_struktural_report