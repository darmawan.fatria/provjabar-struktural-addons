##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import fields, osv
from openerp.tools.translate import _



class pemetaan_jabatan(osv.Model):
    _inherit = 'pemetaan.jabatan'
    #Laporan
    def download_daftar_rotasi(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        print "download daftar rotasi"
        datas = {'data_ids':ids,
                 }
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_rotasi_report',
                        data=datas, context=context)
    def download_daftar_promosi(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        print "download daftar promosi"
        datas = {'data_ids':ids,
                 }
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_pemetaan_struktural_pdf_report.report_pemetaan_struktural_promosi_report',
                        data=datas, context=context)
        

pemetaan_jabatan()
    