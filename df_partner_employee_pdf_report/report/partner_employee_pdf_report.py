from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale
from string import upper


class report_partner_employee_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_partner_employee_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_detail_system_rotasi_data' : self._get_detail_system_rotasi_data,
            'get_detail_manual_rotasi_data' : self._get_detail_manual_rotasi_data,
            'get_detail_all_employee_data' : self._get_detail_all_employee_data,
            'get_format_date' : self._get_format_date,
            'to_upper':self._to_upper,
           
        })
    def _get_detail_all_employee_data(self,data):
        ids = []
        for data_id in data['system_rec_ids']:
            ids.append(data_id)
        for data_id in data['manual_rec_ids']:
            ids.append(data_id)
        obj_data=None
        if data['type']=='rotasi' :
            obj_data=self.pool.get('pemetaan.rotasi.pegawai').browse(self.cr,self.uid,ids)
        elif data['type']=='promosi' :
            obj_data=self.pool.get('pemetaan.promosi.pegawai').browse(self.cr,self.uid,ids)
        return obj_data
    def _get_detail_system_rotasi_data(self,data):
        obj_data=self.pool.get('pemetaan.rotasi.pegawai').browse(self.cr,self.uid,data['system_rec_ids'])
        return obj_data
    def _get_detail_manual_rotasi_data(self,data):
        obj_data=self.pool.get('pemetaan.rotasi.pegawai').browse(self.cr,self.uid,data['manual_rec_ids'])
        return obj_data

    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    
    def _to_upper(self,a_string):
        return a_string and upper(a_string) or ''
    

class wrapped_report_partner_employee(osv.AbstractModel):
    _name = 'report.df_partner_employee_pdf_report.report_partner_employee_report'
    _inherit = 'report.abstract_report'
    _template = 'df_partner_employee_pdf_report.report_partner_employee_report'
    _wrapped_report_class = report_partner_employee_report
