##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import fields, osv
from openerp.tools.translate import _



class pemetaan_jabatan(osv.Model):
    _inherit = 'pemetaan.jabatan'
    #Laporan
    def download_lampiran_rotasi(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        print "download daftar rotasi"
        rotasi_pool = self.pool.get('pemetaan.rotasi.pegawai')
        system_rec_ids  = rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                   ('type', '=', 'system'),
                                   ('active', '=', True)],context=None)
        manual_rec_ids  = rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                   ('type', '=', 'recommendation'),
                                   ('active', '=', True)],context=None)
        datas = {'data_ids':ids,
                 'system_rec_ids':system_rec_ids,
                 'manual_rec_ids':manual_rec_ids,
                 'type':'rotasi'
                 }
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_partner_employee_pdf_report.report_partner_employee_report',
                        data=datas, context=context)
    def download_lampiran_promosi(self, cr, uid, ids, context={}):
        if context is None:
            context = {}
        print "download daftar promosi"
        promosi_pool = self.pool.get('pemetaan.promosi.pegawai')
        system_rec_ids  = promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                   ('type', '=', 'system'),
                                   ('active', '=', True)],context=None)
        manual_rec_ids  = promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                   ('type', '=', 'recommendation'),
                                   ('active', '=', True)],context=None)
        datas = {'data_ids':ids,
                 'system_rec_ids':system_rec_ids,
                 'manual_rec_ids':manual_rec_ids,
                 'type':'promosi'
                 }
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_partner_employee_pdf_report.report_partner_employee_report',
                        data=datas, context=context)
        

pemetaan_jabatan()
    