##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import timedelta
from calendar import monthrange
from mx import DateTime

# ====================== Konfigurasi Berdasarkan Pangkat ================================= #
class res_partner(osv.Model):
    _inherit = 'res.partner'

    def monthdelta(self,d1,d2):
        delta=0
        while True:
            mdays = monthrange(d1.year,d1.month)[1]
            d1 += timedelta(days=mdays)
            if d1 <= d2 :
                delta += 1
            else:
                break
        return delta

    def _get_usia_bln(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}

        usia_bln=0
        today=DateTime.today();
        for record in self.read(cr,uid,ids,["id","tanggal_lahir"],context):
            if record['tanggal_lahir'] :
                tgl_lahir =DateTime.strptime(record['tanggal_lahir'],'%Y-%m-%d')
                usia_bln = self.monthdelta(tgl_lahir,today)
            res[record['id']]=usia_bln
        return res

    def _current_masa_kerja(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_bulan=0
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id)],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today=DateTime.today();
                    awal_masa_kerja = DateTime.strptime(golongan_hist.date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,today)
            res[id]=masa_kerja_bulan
        return res
    def _current_masa_kerja_jabatan(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_bulan=0
            job_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date,id desc")
            if job_ids:
                job_hist=self.pool.get('partner.employee.job.history').browse(cr,uid,job_ids[0])
                masa_kerja_bulan = job_hist.masa_kerja_job_history

            res[id]=masa_kerja_bulan
        return res
    def _current_masa_kerja_jabatan_thn(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_thn=0
            job_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date,id desc")
            if job_ids:
                job_hist=self.pool.get('partner.employee.job.history').browse(cr,uid,job_ids[0])
                masa_kerja_thn = job_hist.masa_kerja_thn_job_history

            res[id]=masa_kerja_thn
        return res

    def _current_masa_kerja_jabatan_color_type(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            warna='white'
            job_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date,id desc")
            if job_ids:
                job_hist=self.pool.get('partner.employee.job.history').browse(cr,uid,job_ids[0])
                masa_kerja_thn = job_hist.masa_kerja_thn_job_history

                #hardcode logic
                if masa_kerja_thn < 2:
                    warna='red'
                elif masa_kerja_thn >= 2 and masa_kerja_thn <=5:
                    warna='yellow'
                elif masa_kerja_thn > 5:
                    warna='green'
            res[id]=warna
        return res
    def _current_masa_kerja_jabatan_bln(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_bln=0
            job_ids=self.pool.get('partner.employee.job.history').search(cr,uid,[('partner_id','=',id)],order="date,id desc")
            if job_ids:
                job_hist=self.pool.get('partner.employee.job.history').browse(cr,uid,job_ids[0])
                masa_kerja_bln = job_hist.masa_kerja_bln_job_history

            res[id]=masa_kerja_bln
        return res

    def _current_masa_kerja_thn(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_thn=0
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today=DateTime.today();
                    awal_masa_kerja = DateTime.strptime(golongan_hist.date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,today)
                    if masa_kerja_bulan>=12:
                        masa_kerja_thn = int(masa_kerja_bulan/12)
            res[id]=masa_kerja_thn
        return res
    def _current_masa_kerja_bln(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            masa_kerja_sisa_bln=0
            golongan_ids=self.pool.get('partner.employee.golongan.history').search(cr,uid,[('partner_id','=',id),('jenis','=','pns')],order="date asc,id asc")
            if golongan_ids:
                golongan_hist=self.pool.get('partner.employee.golongan.history').browse(cr,uid,golongan_ids[0])
                if golongan_hist.date :
                    today=DateTime.today();
                    awal_masa_kerja = DateTime.strptime(golongan_hist.date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,today)
                    if masa_kerja_bulan>=12:
                        masa_kerja_sisa_bln = int(masa_kerja_bulan % 12)
                    else :
                        masa_kerja_sisa_bln = masa_kerja_bulan
            res[id]=masa_kerja_sisa_bln
        return res
    def _get_nilai_rata2_skp_2thn(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        skp_yearly_pool = self.pool.get('skp.employee.yearly')
        for id in ids:
            nilai_rata2_skp_2thn=0
            skp_ids=skp_yearly_pool.search(cr,uid,[('employee_id','=',id)],order="target_period_year desc")
            if skp_ids:
                for skp_yearly in skp_yearly_pool.browse(cr,uid,skp_ids,context=None):
                    nilai_rata2_skp_2thn+=skp_yearly.nilai_total
            res[id]=nilai_rata2_skp_2thn
        return res
    def _current_pensiun_color_type(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for emp in self.browse(cr,uid,ids,context=None):
            warna='white'
            if emp.eselon_id:
                batas_pensiun = ( emp.eselon_id.batas_pensiun *12)
                usia = emp.usia_bln
                limit = batas_pensiun - usia
                #hardcode logic
                if limit <=3:
                    warna='black'
                elif limit > 3 and limit <=6:
                    warna='purple'
                elif limit > 6 and limit <=12:
                    warna='blue'
            res[emp.id]=warna
        return res

    _columns = {
        'template_jabatan_id'   : fields.many2one('template.jabatan', 'Template Jabatan',readonly=True),
        'informasi_jabatan_id': fields.related('template_jabatan_id','informasi_jabatan_id', type='many2one',relation='informasi.jabatan', string='Informasi Jabatan', store=True,readonly=True),
        'masa_kerja': fields.function(_current_masa_kerja, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja (Total Satuan Bulan)'),
        'masa_kerja_thn': fields.function(_current_masa_kerja_thn, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Tahun'),
        'masa_kerja_bln': fields.function(_current_masa_kerja_bln, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Bulan'),
        'skp_ids': fields.one2many('skp.employee.yearly', 'employee_id', 'SKP',order='target_period_year desc'),
        'nilai_rata2_skp_2thn': fields.function(_get_nilai_rata2_skp_2thn, method=True,readonly=True , store=False,
                                         type="float",string='Rata Rata SKP 2 Tahun'),
        'penghargaan_ids': fields.one2many('partner.employee.penghargaan.history', 'partner_id', 'Riwayat Penghargaan',order='date desc'),
        'masa_kerja_jabatan': fields.function(_current_masa_kerja_jabatan, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Jabatan (Total Bulan)'),
        'masa_kerja_jabatan_thn': fields.function(_current_masa_kerja_jabatan_thn, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Jabatan (Tahun)'),
        'masa_kerja_jabatan_bln': fields.function(_current_masa_kerja_jabatan_bln, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Jabatan (Bulan)'),
        'pengalaman_berkarya_ids': fields.one2many('partner.employee.pengalaman.berkarya.history', 'partner_id', 'Riwayat Pengalaman Berkarya',order='date desc'),
        'masa_kerja_jabatan_color_type': fields.function(_current_masa_kerja_jabatan_color_type, method=True,readonly=True , store=False,
                                         type="char",string='Tipe Warna Masa Jabatan'),
        'usia_bln': fields.function(_get_usia_bln, method=True,readonly=True , store=False,
                                         type="integer",string='Usia'),
        'pensiun_color_type': fields.function(_current_pensiun_color_type, method=True,readonly=True , store=False,
                                         type="char",string='Tipe Warna Pensiun'),
        'nilai_rekomendasi_jabatan_struktural'     : fields.float('Nilai Jabatan Struktural'),
    }

    def get_pengalaman_berkarya_group(self, cr, uid, partner_id, context=None):
        pengalaman_group= []
        domain = partner_id,
        query = """ Select pengalaman_type_id,kegiatan_type_id,count(id) cnt from partner_employee_pengalaman_berkarya_history
                where partner_id = %s
                group by pengalaman_type_id,kegiatan_type_id
        """
        cr.execute(query,domain)
        result = cr.fetchall()
        for pengalaman_type_id,kegiatan_type_id,cnt in result:
            new_dict = {}
            new_dict['pengalaman_type_id'] = pengalaman_type_id
            new_dict['kegiatan_type_id'] = kegiatan_type_id
            new_dict['cnt'] = cnt
            pengalaman_group.append(new_dict)

        return pengalaman_group;



    def update_pemetaan_jabatan(self, cr, uid, ids,context=None):
        print "pemetaan dalam jabatan...."
        return True
res_partner()
