##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
from openerp.osv import fields, osv
from datetime import timedelta
from calendar import monthrange
from mx import DateTime

#history -----------
class partner_employee_hukuman_disiplin_history(osv.Model):
    _inherit = "partner.employee.hukuman.disiplin.history"
    _columns = {
        'name'      : fields.selection([('ringan', 'Ringan'),
                                        ('sedang_kgb', 'Sedang (Penundaan Kenaikan Gaji Berkala)'),
                                        ('sedang_penurunan', 'Sedang (Penurunan Gaji Dan/Atau Pangkat)'),
                                        ('berat', 'Berat'),
                                        ], 'Jenis Hukuman Disiplin', required=True),

    }
    _order = "date desc"
partner_employee_hukuman_disiplin_history()
class partner_employee_penghargaan_history(osv.Model):
    _name = "partner.employee.penghargaan.history"
    _description = "Riwayat Penghargaan"
    _columns = {
        'name': fields.char("No Bukti/SK", size=100,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'penghargaan_type_id': fields.many2one('penghargaan.type', 'Jenis Penghargaan',required=True),
        'date': fields.date("Tanggal Penerimaan Penghargaan", required=True ),
    }
    _order = "date desc"
partner_employee_penghargaan_history()
class partner_employee_job_history(osv.Model):
    _inherit = "partner.employee.job.history"
    def monthdelta(self,d1,d2):
        delta=0
        while True:
            mdays = monthrange(d1.year,d1.month)[1]
            d1 += timedelta(days=mdays)
            if d1 <= d2 :
                delta += 1
            else:
                break
        return delta

    def _current_masa_kerja(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for job_history in self.browse(cr,uid,ids,context=None):
            masa_kerja_bulan=0
            next_date = None
            job_date = job_history.date

            next_job_history_id=self.search(cr,uid,[('partner_id','=',job_history.partner_id.id),('date','>',job_date)],order="date asc",limit=1)
            #print job_history.job_id_history.name," - ",job_date," : ",next_job_history_id
            if next_job_history_id:
                for next_job_history in self.browse(cr,uid,next_job_history_id,context=None):
                    next_date = DateTime.strptime(next_job_history.date,'%Y-%m-%d')
            else :
                next_date = DateTime.today();
            #print "next date : ",next_date
            if next_date :
                    awal_masa_kerja = DateTime.strptime(job_date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,next_date)
            res[job_history.id]=masa_kerja_bulan
        return res
    def _current_masa_kerja_thn(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for job_history in self.browse(cr,uid,ids,context=None):
            masa_kerja_thn=0
            next_date = None
            job_date = job_history.date

            next_job_history_id=self.search(cr,uid,[('partner_id','=',job_history.partner_id.id),('date','>',job_date)],order="date asc",limit=1)
            #print job_history.job_id_history.name," - ",job_date," : ",next_job_history_id
            if next_job_history_id:
                for next_job_history in self.browse(cr,uid,next_job_history_id,context=None):
                    next_date = DateTime.strptime(next_job_history.date,'%Y-%m-%d')
            else :
                next_date = DateTime.today();
            #print "next date : ",next_date
            if next_date :
                    awal_masa_kerja = DateTime.strptime(job_date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,next_date)
                    if masa_kerja_bulan>=12:
                         masa_kerja_thn= int(masa_kerja_bulan/12)
            res[job_history.id]=masa_kerja_thn
        return res
    def _current_masa_kerja_bln(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for job_history in self.browse(cr,uid,ids,context=None):
            masa_kerja_sisa_bln=0
            next_date = None
            job_date = job_history.date

            next_job_history_id=self.search(cr,uid,[('partner_id','=',job_history.partner_id.id),('date','>',job_date)],order="date asc",limit=1)
            #print job_history.job_id_history.name," - ",job_date," : ",next_job_history_id
            if next_job_history_id:
                for next_job_history in self.browse(cr,uid,next_job_history_id,context=None):
                    next_date = DateTime.strptime(next_job_history.date,'%Y-%m-%d')
            else :
                next_date = DateTime.today();
            #print "next date : ",next_date
            if next_date :
                    awal_masa_kerja = DateTime.strptime(job_date,'%Y-%m-%d')
                    masa_kerja_bulan = self.monthdelta(awal_masa_kerja,next_date)
                    if masa_kerja_bulan>=12:
                        masa_kerja_sisa_bln = int(masa_kerja_bulan % 12)
                    else :
                        masa_kerja_sisa_bln = masa_kerja_bulan
            res[job_history.id]=masa_kerja_sisa_bln
        return res


    _columns = {
        'masa_kerja_job_history': fields.function(_current_masa_kerja, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja'),
        'masa_kerja_thn_job_history': fields.function(_current_masa_kerja_thn, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Tahun'),
        'masa_kerja_bln_job_history': fields.function(_current_masa_kerja_bln, method=True,readonly=True , store=False,
                                         type="integer",string='Masa Kerja Bulan'),

    }
    _order = "date desc"
partner_employee_job_history()
#partner.employee.pengalaman.berkarya.history
class partner_employee_pengalaman_berkarya_history(osv.Model):
    _name = "partner.employee.pengalaman.berkarya.history"
    _description = "Riwayat Pengalaman Berkarya"
    _columns = {
        'name': fields.char("No Bukti/SK/Sertifikat", size=100,required=True ),
        'partner_id': fields.many2one('res.partner', 'Data Pegawai',required=True),
        'pengalaman_type_id': fields.many2one('pengalaman.type', 'Jenis Pengalaman',required=True),
        'kegiatan_type_id': fields.many2one('kegiatan.type', 'Jenis Kegiatan',required=True,domain="[('pengalaman_type_id','=',pengalaman_type_id)]"),
        'date': fields.date("Tanggal Kegiatan", required=True ),
        'tempat': fields.char("Tempat", size=100,required=False ),
        'description':fields.text("Keterangan Lain")
    }
    _order = "date desc"
partner_employee_pengalaman_berkarya_history()
#config
class partner_employee_diklat_type(osv.Model):
    _inherit = "diklat.type"

    _columns = {
        'code': fields.char("Kode", size=15,required=False ),
    }
partner_employee_diklat_type()
class partner_employee_eselon(osv.Model):
    _inherit = "partner.employee.eselon"

    _columns = {
        'batas_pensiun': fields.integer("Batas Pensiun (Tahun",),
        'color': fields.char("Warna", size=25 ),

    }
partner_employee_eselon()
class partner_employee_penghargaan_type(osv.Model):
    _name = "penghargaan.type"
    _description = "Penghargaan Pegawai"
    _columns = {
        'code': fields.char("Kode Penghargaan ", size=8, required=True),
        'name': fields.char("Nama Penghargaan ", size=45,required=True ),
        'sequence': fields.integer("Sequence",),
        'description': fields.text("Deskripsi" )
    }

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_penghargaan_type()
class partner_employee_pengalaman_type(osv.Model):
    _name = "pengalaman.type"
    _description = "Pengalaman Pegawai"
    _columns = {
        'code': fields.char("Kode Penghargaan ", size=8, required=True),
        'name': fields.char("Nama Penghargaan ", size=95,required=True ),
        'sequence': fields.integer("Sequence",),
        'description': fields.text("Deskripsi" )
    }

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_pengalaman_type()
class partner_employee_kegiatan_type(osv.Model):
    _name = "kegiatan.type"
    _description = "Uraian Pengalaman Pegawai"
    _columns = {
        'pengalaman_type_id'   : fields.many2one('pengalaman.type', 'Jenis Pengalaman',required=True),
        'code': fields.char("Kode Penghargaan ", size=8, required=True),
        'name': fields.char("Nama Penghargaan ", size=226,required=True ),
        'sequence': fields.integer("Sequence",),
        'description': fields.text("Deskripsi" )
    }

    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_kegiatan_type()
