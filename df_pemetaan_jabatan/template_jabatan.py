##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv


# ====================== Konfigurasi Berdasarkan Pangkat ================================= #
class template_jabatan(osv.Model):
    _inherit = "template.jabatan"
    _description ="Template Jabatan"
    _columns = {
        'parent_id'     : fields.many2one('template.jabatan', 'Parent'),
        'employee_id'   : fields.many2one('res.partner', 'Pegawai',domain=[('employee','=',True)],),
        'informasi_jabatan_id'   : fields.many2one('informasi.jabatan', 'Informasi Jabatan',),
    }

    def update_pemetaan_jabatan(self, cr, uid, ids,context=None):
        print "pemetaan dalam jabatan...."
        return True
template_jabatan()
