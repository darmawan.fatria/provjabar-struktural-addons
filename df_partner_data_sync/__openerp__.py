##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
{
    "name": "Data Sinkronisasi Kepegawaian",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Data Kepegawaian/Struktural",
    "description": "Data Sinkronisasi Kepegawaian",
    "website" : "-",
    "license" : "",
    "depends": ['df_partner_employee','df_pemetaan_struktural'],
    'data': [
        "app_auto_sync_settings_view.xml"
             ],
    'installable': True,
    'active': False,
}
