/**
 * Created by adrianto on 17/05/16.
 */
/**
 * Created by adrianto on 17/05/16.
 */
  function init() {
      var $ = go.GraphObject.make;  // for conciseness in defining templates

      myDiagram =
        $(go.Diagram, "myDiagramDiv", // must be the ID or reference to div
          {
            padding: 5,
            hasHorizontalScrollbar: true,
            hasVerticalScrollbar: true,
            initialContentAlignment: go.Spot.Center,
            maxSelectionCount: 1, // users can select only one part at a time
            "undoManager.isEnabled": true // enable undo &amp; redo
          });

      // when the document is modified, add a "*" to the title and enable the "Save" button
      myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
          if (idx < 0) document.title += "*";
        } else {
          if (idx >= 0) document.title = document.title.substr(0, idx);
        }
      });

      // when a node is double-clicked, add a child to it
      function nodeDoubleClick(e, obj) {
        var clicked = obj.part;
        if (clicked !== null) {
          var thisemp = clicked.data;
          myDiagram.startTransaction("add employee");
          var nextkey = (myDiagram.model.nodeDataArray.length + 1).toString();
          var newemp = { key: nextkey, name: "(new person)", title: "", parent: thisemp.key };
          var newlink = { from: thisemp.key, to: nextkey};
          myDiagram.model.addNodeData(newemp);
          myDiagram.model.addLinkData(newlink);
          myDiagram.commitTransaction("add employee");
        }
      }

      // this is used to determine feedback during drags
      function mayWorkFor(node1, node2) {
        if (!(node1 instanceof go.Node)) return false;  // must be a Node
        if (node1 === node2) return false;  // cannot work for yourself
        if (node2.isInTreeOf(node1)) return false;  // cannot work for someone who works for you
        return true;
      }

      // // This function provides a common style for most of the TextBlocks.
      // // Some of these values may be overridden in a particular TextBlock.
      function textStyle() {
        return { font: "9pt  Segoe UI,sans-serif", stroke: "black" };
      }

      // This converter is used by the Picture.
      function findHeadShot(key) {
        if (key > 16) return ""; // There are only 16 images on the server
        return "images/HS"+key+".png";
      };

      // define the Node template
      myDiagram.nodeTemplate =
        $(go.Node, "Auto",
          //new go.Binding("location", "loc"),
          new go.Binding("location", "loc", go.Point.parse),
          // for sorting, have the Node.text be the data.name
          new go.Binding("text", "name"),
          // bind the Part.layerName to control the Node's layer depending on whether it isSelected
          new go.Binding("layerName", "isSelected", function(sel) { return sel ? "Foreground" : ""; }).ofObject(),
          // define the node's outer shape
          $(go.Shape, "Rectangle",
            {
              name: "SHAPE", fill: "white", stroke: "#006161" , strokeWidth: 4, height: 80, width: 225,
              // set the port properties:
              portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer"
            }),
          $(go.Panel, "Horizontal",
            
            $(go.Picture,
              {
                name: 'Picture',
                desiredSize: new go.Size(50, 70),
                margin: new go.Margin(10, 5, 10, 5),
                alignment: go.Spot.Left,
                //source: 'data.....'
              },
              new go.Binding("source","img").makeTwoWay()),
              //new go.Binding("source")),
            // define the panel where the text will appear
            $(go.Panel, "Table",
              {
                maxSize: new go.Size(160, 999),
                width: 160,
                // margin: new go.Margin(10, 0, 5, 0),
                defaultAlignment: go.Spot.Left
              },
              $(go.RowColumnDefinition, { column: 2, width: 3 }),
              $(go.TextBlock, textStyle(),  // the name
                {
                  alignment: go.Spot.TopLeft,
                  name: "NAME",
                  row: 0, column: 0, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  font: "10pt Segoe UI,sans-serif",
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(0, 10, 0, 0)
                },
                new go.Binding("text", "name").makeTwoWay()),
              // $(go.TextBlock, "Title: ", textStyle(),
              //  { row: 1, column: 0 }),
              $(go.TextBlock, textStyle(),
                {
                  name: "TITLE",
                  row: 1, column: 0, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(2, 20, 0, 0)
                },
                new go.Binding("text", "title").makeTwoWay())
            )  // end Table Panel
          ) // end Horizontal Panel
        );  // end Node

      // define the Link template
      myDiagram.linkTemplate =
        $(go.Link, 
          { 
            routing: go.Link.AvoidsNodes, //go.Link.Orthogonal,
            reshapable: true, 
            corner: 5, 
            relinkableFrom: true, 
            relinkableTo: true
            // fromSpot: go.Spot.Bottom,
            // toSpot: go.Spot.Top
          },
          new go.Binding("points").makeTwoWay(),
          new go.Binding("fromSpot", "fromSpot", go.Spot.parse),
          new go.Binding("toSpot", "toSpot", go.Spot.parse),
          $(go.Shape, { strokeWidth: 4, stroke: "#00a4a4" }));  // the link shape

      // read in the JSON-format data from the "mySavedModel" element
      load();


      // support editing the properties of the selected person in HTML
      if (window.Inspector) myInspector = new Inspector('myInspector', myDiagram,
        {
          properties: {
            'key': { readOnly: true }
          }
    });
  }
  
  // Show the diagram's model in JSON format
  function save() {
      document.getElementById("mySavedModel").value = myDiagram.model.toJson();
      myDiagram.isModified = false;
  }

  function load() {
      openerp.jsonRpc("/peta_jabatan_json", 'call', {})
                .done(function(res){
                    console.log(res[0]);
                    console.log(res[1]);
                    var nodeDataArray=res[0];
                    var linkDataArray =res[1];
                    myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
                });
      openerp.jsonRpc("/peta_jabatan_json", 'call', {})
                .done(function(res){
                    //console.log(res);

                });
  }