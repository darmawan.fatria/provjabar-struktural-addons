/**
 * Created by adrianto on 17/05/16.
 */
  //global

  function init(nodeDataArray,linkDataArray,nodeHeight,nodeWidth) { //,detailDataArray
      var limitData = 4; //total data shown in table
      //console.log('init....');
      $jq('#myModal').on('show.bs.modal', function() {
          var modalWidth = ($jq(window).width()*0.9)+'px';
          $jq('.modal-content').css('max-width',modalWidth);
      });

      var $ = go.GraphObject.make;  // for conciseness in defining templates

      myDiagram =
        $(go.Diagram, "myDiagramDiv", // must be the ID or reference to div
          {
            padding: 5,
            hasHorizontalScrollbar: true,
            hasVerticalScrollbar: true,
            initialContentAlignment: go.Spot.Center,
            maxSelectionCount: 1, // users can select only one part at a time
            "undoManager.isEnabled": true // enable undo &amp; redo
          });

      // when the document is modified, add a "*" to the title and enable the "Save" button
      myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("SaveButton");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
          if (idx < 0) document.title += "*";
        } else {
          if (idx >= 0) document.title = document.title.substr(0, idx);
        }
      });

      // when a node is double-clicked, add a child to it
      function nodeDoubleClick(e, obj) {
        var clicked = obj.part;
        if (clicked !== null) {
          var thisemp = clicked.data;
          myDiagram.startTransaction("add employee");
          var nextkey = (myDiagram.model.nodeDataArray.length + 1).toString();
          var newemp = { key: nextkey, name: "(new person)", title: "", parent: thisemp.key };
          var newlink = { from: thisemp.key, to: nextkey};
          myDiagram.model.addNodeData(newemp);
          myDiagram.model.addLinkData(newlink);
          myDiagram.commitTransaction("add employee");
        }
      }

      // when a node is clicked, add a child to it
      function showModal(e, obj) {
        var clicked = obj.part;
        if (clicked !== null) {
          var key = clicked.data.key;
          var urlJson = "/detail_pegawai_json";
          // for (var i=0;i<detailDataArray.length;i++){
          //       if(detailDataArray[i].key == key){
          //           detailData = detailDataArray[i];
          //           continue;
          //       }
          // }
          openerp.jsonRpc(urlJson,'call', {'key': key})
              .done(function(res){
                  var detailData = res;
                  tableDataClear();

                  //top table
                  $jq('#pegawaiImg').attr('src',detailData.pegawaiImage);
                  $jq('#pegawaiName').html(detailData.pegawaiName);
                  $jq('#pegawaiNIP').html(detailData.pegawaiNIP);
                  $jq('#pegawaiOPD').html(detailData.pegawaiOPD);
                  $jq('#biro').html(detailData.biro);
                  $jq('#unitKerja').html(detailData.unitKerja);
                  $jq('#jenJabatan').html(detailData.jenJabatan);
                  $jq('#eselon').html(detailData.eselon);
                  $jq('#pangGol').html(detailData.pangGol);
                  $jq('#jabatan').html(detailData.jabatan);


                  //tab 1
                  var dataRKJ = detailData.dataRKJ;
                  tableData(dataRKJ.dataRJ,dataRKJ.dataRJ_col,'RJ',limitData);
                  tableData(dataRKJ.dataRK,dataRKJ.dataRK_col,'RK',limitData);

                  //tab 2
                  var dataRPD = detailData.dataRPD;
                  tableData(dataRPD.dataRP,dataRPD.dataRP_col,'RP',limitData);
                  tableData(dataRPD.dataRBK,dataRPD.dataRBK_col,'RBK',limitData);
                  tableData(dataRPD.dataRDK,dataRPD.dataRDK_col,'RDK',limitData);
                  tableData(dataRPD.dataRDF,dataRPD.dataRDF_col,'RDF',limitData);
                  tableData(dataRPD.dataRDT,dataRPD.dataRDT_col,'RDT',limitData);

                  //tab 3
                  var dataRL = detailData.dataRL;
                  tableData(dataRL.dataRPh,dataRL.dataRPh_col,'RPh',limitData);
                  tableData(dataRL.dataRS,dataRL.dataRS_col,'RS',limitData);
                  tableData(dataRL.dataRPB,dataRL.dataRPB_col,'RPB',limitData);
                  tableData(dataRL.dataRHD,dataRL.dataRHD_col,'RHD',limitData);

                  //tab 4
                  var dataIU = detailData.dataIU;
                  tableIU(dataIU);

                  //tab 5
                  var dataSKP = detailData.dataSKP;
                  tableData(dataSKP,detailData.dataSKP_col,'SKP',limitData);

                  //tab 6 No Need..
                  //$jq('#tempJabatan').html(detailData.dataPJ.tempJabatan);
                  //$jq('#infoJabatan').html(detailData.dataPJ.infoJabatan);

                  $jq("#myModal").modal();
              });
        }
      }

      function tableDataClear(){
        $jq('tbody[id*="tbody"]').html('');
      }

      function tableIU(dataArray){
        for(var j=0;j<Object.keys(dataArray).length;j++){
          var prop = Object.keys(dataArray)[j];
          var propVal = dataArray[prop];
          if(j<(Object.keys(dataArray).length-5))
            $jq('#'+prop).html(propVal);
          else {
            if(propVal == 'true')
              $jq('#'+prop).prop( "checked", true );
          }
        }
      }

      function tableData(data,data_col,tableName,tableLength) {
          var dataArray = JSON.parse(data);
          if (dataArray.length > 0) {
              for (var i = 0; i < dataArray.length; i++) {
                  var tdAppend = '';
                  for (var j = 0; j < Object.keys(dataArray[0]).length; j++) {
                      var prop = Object.keys(dataArray[i])[j];
                      var propVal = dataArray[i][prop];
                      tdAppend += '<td id="' + prop + tableName + '_' + i + '">' + propVal + '</td>';
                  }
                  $jq('#tbody' + tableName).append('<tr id="tr' + tableName + '_' + i + '">' + tdAppend + '</tr>');
              }
              if (dataArray.length <= tableLength) {
                  for (var i = dataArray.length; i <= (tableLength - dataArray.length); i++) {
                      var tdAppend = '';
                      for (var j = 0; j < Object.keys(dataArray[0]).length; j++) {
                          tdAppend += '<td></td>';
                      }
                      $jq('#tbody' + tableName).append('<tr id="tr' + tableName + '_' + i + '">' + tdAppend + '</tr>');
                  }
              }
          }
          else {
              for (var i = 0; i < tableLength; i++) {
                  var tdAppend = '';
                  for(var j=0;j<data_col;j++){
                    tdAppend += '<td></td>';
                  }
                  $jq('#tbody' + tableName).append('<tr id="tr' + tableName + '_' + i + '">' + tdAppend + '</tr>');
              }
          }
      }

      // this is used to determine feedback during drags
      function mayWorkFor(node1, node2) {
        if (!(node1 instanceof go.Node)) return false;  // must be a Node
        if (node1 === node2) return false;  // cannot work for yourself
        if (node2.isInTreeOf(node1)) return false;  // cannot work for someone who works for you
        return true;
      }

      // // This function provides a common style for most of the TextBlocks.
      // // Some of these values may be overridden in a particular TextBlock.
      function textStyle() {
        return { font: "9pt  Segoe UI,sans-serif", stroke: "black" };
      }

      // This converter is used by the Picture.
      function findHeadShot(key) {
        if (key > 16) return ""; // There are only 16 images on the server
        return "images/HS"+key+".png";
      };

      // define the Node template
      myDiagram.nodeTemplate =
        $(go.Node, "Auto",
          {
            click: showModal
          },
          //new go.Binding("location", "loc"),
          new go.Binding("location", "loc", go.Point.parse),
          // for sorting, have the Node.text be the data.name
          new go.Binding("text", "name"),
          // bind the Part.layerName to control the Node's layer depending on whether it isSelected
          new go.Binding("layerName", "isSelected", function(sel) { return sel ? "Foreground" : ""; }).ofObject(),
          // define the node's outer shape
          $(go.Shape, "Rectangle",
            {
              name: "SHAPE", fill: "white", stroke: "#006161" , strokeWidth: 4, height: nodeHeight, width: nodeWidth,
              // set the port properties:
              portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer"
            }),
          $(go.Panel, "Horizontal",
            
            $(go.Picture,
              {
                name: 'Picture',
                desiredSize: new go.Size(50, 70),
                margin: new go.Margin(10, 5, 10, 5),
                alignment: go.Spot.Left,
                //source: 'data.....'
              },
              {
                mouseEnter: function (e, node) {
                  node.cursor = "pointer"
                }
              },
              new go.Binding("source","img").makeTwoWay()),
              //new go.Binding("source")),
            // define the panel where the text will appear
            $(go.Panel, "Table",
              {
                maxSize: new go.Size(160, 999),
                width: 160,
                // margin: new go.Margin(10, 0, 5, 0),
                defaultAlignment: go.Spot.Left
              },
              // $(go.RowColumnDefinition, { column: 2, width: 3 }),
              $(go.TextBlock, textStyle(),  // the name
                {
                  alignment: go.Spot.TopLeft,
                  name: "NAME",
                  row: 0, column: 0, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  font: "10pt Segoe UI,sans-serif",
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(0, 10, 0, 0)
                },
                new go.Binding("text", "name").makeTwoWay()
              ),
              // $(go.TextBlock, "Title: ", textStyle(),
              //  { row: 1, column: 0 }),
              $(go.TextBlock, textStyle(),
                {
                  name: "TITLE",
                  row: 1, column: 0, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(2, 10, 0, 0)
                },
                new go.Binding("text", "title").makeTwoWay()
              ),
              $(go.TextBlock, textStyle(),
                {
                  name: "PANGKAT",
                  row: 2, column: 0, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(2, 10, 0, 0)
                },
                new go.Binding("text", "pangkat").makeTwoWay()
              ),
              $(go.TextBlock, textStyle(),
                {
                  name: "MASAJABATAN",
                  row: 2, column: 4, columnSpan: 5,
                  wrap: go.TextBlock.WrapFit,
                  editable: true, isMultiline: false,
                  minSize: new go.Size(10, 16),
                  margin: new go.Margin(2, 0, 0, 42)
                },
                new go.Binding("text", "masa").makeTwoWay()
              ),
              $(go.Shape, "Rectangle",
                {
                  name: "JKOSONG",
                  row: 3, column: 0, columnSpan: 2,
                  stroke: null,
                  height: 10, width: 10,
                  margin: new go.Margin(0, 1, 0, 0),
                  visible: false
                },
                new go.Binding("visible", "jkCol", function(v) {return true;}),
                new go.Binding("fill", "jkCol")
              ),
              $(go.Shape, "Rectangle",
                {
                  name: "JPENSIUN",
                  row: 3, column: 2, columnSpan: 2,
                  stroke: null,
                  height: 10, width: 10,
                  margin: new go.Margin(0, 1, 0, 0),
                  visible: false
                },
                new go.Binding("visible", "jpCol", function(v) {return true;}),
                new go.Binding("fill", "jpCol")
              ),
              $(go.Shape, "Rectangle",
                {
                  name: "JSAMA",
                  row: 3, column: 4, columnSpan: 2,
                  stroke: null,
                  height: 10, width: 10,
                  margin: new go.Margin(0, 1, 0, 0),
                  visible: false
                },
                new go.Binding("visible", "jsCol", function(v) {return true;}),
                new go.Binding("fill", "jsCol")
              )
            )  // end Table Panel
          ) // end Horizontal Panel
        );  // end Node

      // define the Link template
      myDiagram.linkTemplate =
        $(go.Link, 
          { 
            routing: go.Link.AvoidsNodes, //go.Link.Orthogonal,
            reshapable: true, 
            corner: 5, 
            relinkableFrom: true, 
            relinkableTo: true
            // fromSpot: go.Spot.Bottom,
            // toSpot: go.Spot.Top
          },
          new go.Binding("points").makeTwoWay(),
          new go.Binding("fromSpot", "fromSpot", go.Spot.parse),
          new go.Binding("toSpot", "toSpot", go.Spot.parse),
          $(go.Shape, { strokeWidth: 4, stroke: "#00a4a4" }));  // the link shape

      // read in the JSON-format data from the "mySavedModel" element
      load(nodeDataArray,linkDataArray);


      // support editing the properties of the selected person in HTML
      if (window.Inspector) myInspector = new Inspector('myInspector', myDiagram,
        {
          properties: {
            'key': { readOnly: true }
          }
    });
  }
  
  // Show the diagram's model in JSON format
  function save() {
      document.getElementById("mySavedModel").value = myDiagram.model.toJson();
      myDiagram.isModified = false;
  }

  function load(nodeDataArray, linkDataArray) {
      myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
  }
