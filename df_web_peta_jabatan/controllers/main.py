# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.addons.web import http
from openerp.addons.web.http import request
from collections import OrderedDict
import base64
import json

node_width, node_height = 225, 110
node_distance_x, node_distance_y = 300, 140
kabid_mul_factor = 3
border_thickness = 2

class WebsitePetaJabatan(http.Controller):

    @http.route(['/peta_jabatan',
                 '/peta_jabatan/<int:param_company_id>',
                 ], type='http', auth='public',
                website=True)
    def peta_jabatan_view(self, param_company_id=165, **post):
        cr, uid, context = request.cr, request.uid, request.context
        pemetaan_pool = request.registry['pemetaan.jabatan']
        company_pool = request.registry['res.company']
        company_id = param_company_id

        domain = []
        if company_id:
            domain.append(('company_id', '=', company_id))
        domain.append(('level', '=', 1))

        pemetaan_ids = pemetaan_pool.search(cr, uid, domain, context=context)
        pemetaan_jabatan_list = pemetaan_pool.browse(cr, uid, pemetaan_ids, context=context)

        company = company_pool.browse(cr, uid, company_id, context=context)

        data = {
            'pemetaan_jabatan_list': pemetaan_jabatan_list,
            'opd': company
        }

        return request.website.render("df_web_peta_jabatan.peta_jabatan_view", data)

    @http.route('/peta_jabatan_json', type="json")
    def generate_peta_jabatan_json(self,param_company_id):
        cr, uid, context = request.cr, request.uid, request.context
        pemetaan_pool = request.registry['pemetaan.jabatan']
        company_id = param_company_id
        domain = []

        if company_id:
            domain.append(('company_id', '=', company_id))
        domain.append(('level', '=', 1))
        pemetaan_ids = pemetaan_pool.search(cr, uid, domain, context=context,order='sequence')
        arr_list = []
        linkDetail = []

        for root in pemetaan_pool.browse(cr, uid, pemetaan_ids, context=context):
            dic_l1 = {
                'key': root.id,
                'name': root.employee_id.name or '-',
                'title': root.name,
                'loc': self.get_loc_by_key(-1,-1,root,-1),
                'img':'data:image/png;base64,'+root.employee_id.image_medium,
            }

            if root.employee_id:
                dic_l1.update({
                    "jsCol":root.employee_id.masa_kerja_jabatan_color_type,
                    "jpCol":root.employee_id.pensiun_color_type,
                    'pangkat': root.employee_id.golongan_id.name or '-',
                    'masa': str(root.employee_id.masa_kerja_jabatan_thn) + ' Thn ' +str(root.employee_id.masa_kerja_jabatan_thn) + ' Bln' or '-',
                })
            arr_list.append(dic_l1)
            parent_idx =root.id
            l2_idx = 0
            for l2 in root.child_ids:
                img_binary = False
                if l2.employee_id:
                    img_binary = 'data:image/png;base64,' + l2.employee_id.image_medium

                dic_l2 = {
                    'key': l2.id,
                    'parent': parent_idx,
                    'name': l2.employee_id.name or '-',
                    'title': l2.name,
                    'loc': self.get_loc_by_key(-1, -1, l2, l2_idx),
                    'img': img_binary,
                }

                link_l2 = {
                    'from': parent_idx,
                    'to': l2.id,
                    'fromSpot': 'Bottom',
                    'toSpot': 'Top'
                }
                if not l2.is_secretary:
                    link_l2.update({
                        'points': [
                            node_distance_x + node_width / 2 + border_thickness,
                            node_height + 2 * border_thickness,
                            node_distance_x + node_width / 2 + border_thickness,
                            kabid_mul_factor * node_distance_y - (node_distance_y - node_height) / 2,
                            node_width / 2 + border_thickness + l2_idx * node_distance_x,
                            kabid_mul_factor * node_distance_y - (node_distance_y - node_height) / 2,
                            node_width / 2 + border_thickness + l2_idx * node_distance_x,
                            kabid_mul_factor * node_distance_y
                        ]
                    })
                if l2.employee_id:
                    dic_l2.update({
                        "jsCol":l2.employee_id.masa_kerja_jabatan_color_type,
                         "jpCol":l2.employee_id.pensiun_color_type,
                         'pangkat': l2.employee_id.golongan_id.name or '-',
                        'masa': str(l2.employee_id.masa_kerja_jabatan_thn) + ' Thn ' +str(l2.employee_id.masa_kerja_jabatan_thn) + ' Bln' or '-',
                    })
                arr_list.append( dic_l2 )
                linkDetail.append(link_l2)

                l2_parent_idx = l2.id
                l3_idx = 0
                for l3 in l2.child_ids:
                    img_binary=False
                    if l3.employee_id :
                        img_binary = 'data:image/png;base64,'+l3.employee_id.image_medium

                    dic_l3={
                    'key': l3.id,
                    'parent': l2_parent_idx,
                    'name': l3.employee_id.name or '-',
                    'title': l3.name,
                    'loc': self.get_loc_by_key(l2,l2_idx,l3,l3_idx),
                    'img': img_binary,
                    }

                    link_l3 = {
                        'from': l2_parent_idx,
                        'to': l3.id,
                    }
                    if l2.is_secretary:
                        link_l3.update({
                            'fromSpot': 'Bottom',
                            'toSpot': 'Top'
                        })
                    else:
                        link_l3.update({
                            'fromSpot': 'Right',
                            'toSpot': 'Right'
                        })

                    if l3.employee_id:
                        dic_l3.update({
                            "jsCol":l3.employee_id.masa_kerja_jabatan_color_type,
                            "jpCol":l3.employee_id.pensiun_color_type,
                            'pangkat': l3.employee_id.golongan_id.name or '-',
                            'masa': str(l3.employee_id.masa_kerja_jabatan_thn) + ' Thn ' + str(l3.employee_id.masa_kerja_jabatan_thn) + ' Bln' or '-',

                        })
                    arr_list.append(dic_l3)
                    linkDetail.append(link_l3)

                    l3_idx += 1
                if not l2.is_secretary:
                    l2_idx += 1
        return arr_list, linkDetail, node_height, node_width

    @http.route('/detail_pegawai_json', type="json")
    def get_json_detail(self,key):
        cr, uid, context = request.cr, request.uid, request.context
        pemetaan_pool = request.registry['pemetaan.jabatan']
        content = pemetaan_pool.browse(cr, uid, key, context=context)
        return self.get_json_detail_format(content.id, content.name, content.employee_id)

    def get_json_detail_format(self,key,jabatan,data):
        img_binary = 'data:image/png;base64,'+data.image_medium
        limit_data = 4 - 1  #limit 4 data
        json_format={"key": key,
                     "pegawaiImage": img_binary,
                     "pegawaiName": data.fullname,
                     "pegawaiNIP": data.nip,
                     "pegawaiOPD": data.company_id.name,
                     "biro": "",
                     "unitKerja": data.department_id and data.department_id.name or '',
                     "jenJabatan": data.job_type,
                     "eselon": data.eselon_id and data.eselon_id.name or '',
                     "pangGol": data.golongan_id and data.golongan_id.name or '',
                     "jabatan": jabatan,
                     "pejPenilai": "-",
                     "nipPejPenilai": "-",
                     "atasPejPenilai": "-",
                     "nipAtasPejPenilai": "-",
                     }

        info_lain_dict = {
                    "jenKelamin":data.jenis_kelamin,
                    "tmptLahir": data.tempat_lahir,
                    "tglLahir": data.tanggal_lahir,
                    "usia": data.usia,
                    "agama": data.agama,
                    "masaJabatanB":data.masa_kerja_bln ,
                    "masaJabatanT": data.masa_kerja_thn,
                    "alamat": data.street,
                    "telepon":data.phone,
                    "hp": data.mobile,
                    "email": data.email,
                    }

        arr_len = len(data.job_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_jabatan_arr = '['
        for index, jb in enumerate(data.job_id_history, start=0):
            riwayat_jabatan_dict = OrderedDict([
                ("colJenJabatan", jb.job_type),
                ("colEselon", jb.eselon_id.name),
                ("colJabatan", jb.job_id_history.name),
                ("colUnitKerja", jb.department_id.name),
                ("colNoSK", jb.name),
                ("colOPD", jb.company_name),
                ("colTmtJabatan", jb.date)
            ])
            riwayat_jabatan_arr += self.dump_str_json(riwayat_jabatan_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_jabatan_arr += "]"

        arr_len = len(data.golongan_id_history) - 1
        if arr_len > limit_data :
            arr_len = limit_data
        riwayat_kepankatan_arr = "["
        for index, jb in enumerate(data.golongan_id_history, start=0):
            riwayat_kepangkatan_dict = OrderedDict([
                ("colGol", jb.golongan_id_history.name),
                ("colNoSK", jb.name),
                ("colTmtPangkat", jb.date),
                ("colJenSK", jb.jenis)
            ])
            riwayat_kepankatan_arr += self.dump_str_json(riwayat_kepangkatan_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_kepankatan_arr += "]"

        arr_len = len(data.jenjang_pendidikan_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_pendidikan_arr = "["
        for index, jb in enumerate(data.jenjang_pendidikan_id_history, start=0):
            riwayat_pendidikan_dict = OrderedDict([
                ("colJenPend", jb.jenjang_pendidikan_id_history.name),
                ("colSekolah", jb.nama_sekolah_id_history.name),
                ("colJurusan", jb.jurusan_id_history.name),
                ("colNoIjazah", jb.name),
                ("colIPK", jb.ipk),
                ("colThnLulus",jb.tahun_lulus) ])
            riwayat_pendidikan_arr += self.dump_str_json(riwayat_pendidikan_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_pendidikan_arr += "]"

        arr_len = len(data.bidang_kompetensi_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_kompetensi_arr = "["
        for index, jb in enumerate(data.bidang_kompetensi_id_history, start=0):
            riwayat_kompetensi_dict = OrderedDict([
                ("colBidKomp", jb.type_id.name),
                ("colNoSK", jb.bidang_kompetensi_type_id.name),
                ("colJenjang", jb.name),
                ("colTMTKomp", jb.date)
            ])
            riwayat_kompetensi_arr += self.dump_str_json(riwayat_kompetensi_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_kompetensi_arr += "]"

        arr_len = len(data.diklat_kepemimpinan_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_kepemimpinan_arr = "["
        for index, jb in enumerate(data.diklat_kepemimpinan_id_history, start=0):
            riwayat_kepemimpinan_dict = OrderedDict([
                ("colJenjang", jb.type_id.name),
                ("colNama", jb.name),
                ("colThnLulus", jb.tahun),
                ("colTempat", jb.tempat),
                ("colPenyl", jb.penyelenggara)
            ])
            riwayat_kepemimpinan_arr += self.dump_str_json(riwayat_kepemimpinan_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_kepemimpinan_arr += "]"

        arr_len = len(data.diklat_fungsional_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_fungsional_arr = "["
        for index, jb in enumerate(data.diklat_fungsional_id_history, start=0):
            riwayat_fungsional_dict = OrderedDict([
                ("colJenjang", jb.type_id.name),
                ("colNama", jb.name),
                ("colThnLulus", jb.tahun),
                ("colTempat", jb.tempat),
                ("colPenyl", jb.penyelenggara)
            ])
            riwayat_fungsional_arr += self.dump_str_json(riwayat_fungsional_dict,index,arr_len)
            if index >= arr_len:
                break;
        riwayat_fungsional_arr += "]"

        arr_len = len(data.diklat_teknik_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_teknik_arr = "["
        for index, jb in enumerate(data.diklat_teknik_id_history, start=0):
            riwayat_teknik_dict = OrderedDict([
                ("colJenjang", jb.type_id.name),
                ("colNama", jb.name),
                ("colThnLulus", jb.tahun),
                ("colTempat", jb.tempat),
                ("colPenyl", jb.penyelenggara)
            ])
            riwayat_teknik_arr += self.dump_str_json(riwayat_teknik_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_teknik_arr += "]"

        arr_len = len(data.penghargaan_ids) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_penghargaan_arr = "["
        for index, jb in enumerate(data.penghargaan_ids, start=0):
            riwayat_penghargaan_dict = OrderedDict([
                ("colJenPengh", jb.penghargaan_type_id.name),
                ("colTPP", jb.date),
                ("colNoSK", jb.name)
            ])
            riwayat_penghargaan_arr += self.dump_str_json(riwayat_penghargaan_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_penghargaan_arr += "]"

        arr_len = len(data.pengalaman_berkarya_ids) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_pengalaman_berkarya_arr = "["
        for index, jb in enumerate(data.pengalaman_berkarya_ids, start=0):
            riwayat_pengalaman_berkarya_dict = OrderedDict([
                ("colJenPenga", jb.pengalaman_type_id.name),
                ("colJenKegiatan", jb.kegiatan_type_id.name),
                ("colNoSK", jb.name),
                ("colTglKeg", jb.date),
                ("colTempat", jb.date)
            ])
            riwayat_pengalaman_berkarya_arr += self.dump_str_json(riwayat_pengalaman_berkarya_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_pengalaman_berkarya_arr += "]"

        arr_len = len(data.penataran_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_seminar_arr = "["
        for index, jb in enumerate(data.penataran_id_history, start=0):
            riwayat_seminar_dict = OrderedDict([
                ("colNama", jb.name),
                ("colThnLulus", jb.tahun),
                ("colTempat", jb.tempat),
                ("colPenyl", jb.penyelenggara)
            ])
            riwayat_seminar_arr += self.dump_str_json(riwayat_seminar_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_seminar_arr += "]"

        arr_len = len(data.penataran_id_history) - 1
        if arr_len > limit_data:
            arr_len = limit_data
        riwayat_hukdis_arr = "["
        for index, jb in enumerate(data.hukuman_disiplin_id_history, start=0):
            riwayat_hukdis_dict = OrderedDict([
                ("colTMTHuk", jb.date),
                ("colJenHukDis", jb.name),
                ("colTglAkhHuk", jb.date_end),
                ("colUraian", jb.notes)
            ])
            riwayat_hukdis_arr += self.dump_str_json(riwayat_hukdis_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_hukdis_arr += "]"

        arr_len = len(data.skp_ids) - 1
        if arr_len > limit_data :
            arr_len = limit_data
        riwayat_skp_arr = "["
        for index, jb in enumerate(data.skp_ids, start=0):#
            riwayat_skp_dict = OrderedDict([
                ("periode", jb.target_period_year),
                ("nSKPTBKR", jb.nilai_skp_tambahan_percent),
                ("nPerilakuP", jb.nilai_perilaku_percent),
                ("nTotal", jb.nilai_total),
                ("indNilaiTotal", jb.indeks_nilai_total)
            ])
            riwayat_skp_arr += self.dump_str_json(riwayat_skp_dict,index,arr_len)
            if index >= arr_len:
                break
        riwayat_skp_arr += "]"

        json_format.update({
            "dataRKJ":
                {
                    "dataRJ": riwayat_jabatan_arr,
                    "dataRK": riwayat_kepankatan_arr,
                    "dataRJ_col": 7,
                    "dataRK_col": 4
                }
            ,   "dataRPD":
                {
                    "dataRP": riwayat_pendidikan_arr,
                    "dataRBK": riwayat_kompetensi_arr,
                    "dataRDK": riwayat_kepemimpinan_arr,
                    "dataRDF": riwayat_fungsional_arr,
                    "dataRDT": riwayat_teknik_arr,
                    "dataRP_col": 7,
                    "dataRBK_col": 4,
                    "dataRDK_col": 5,
                    "dataRDF_col": 5,
                    "dataRDT_col": 5
                }
            , "dataRL":
                {
                    "dataRPh": riwayat_penghargaan_arr,
                    "dataRS": riwayat_seminar_arr,
                    "dataRPB": riwayat_pengalaman_berkarya_arr,
                    "dataRHD": riwayat_hukdis_arr,
                    "dataRPh_col": 3,
                    "dataRS_col": 4,
                    "dataRPB_col": 5,
                    "dataRHD_col": 4
                }
            , "dataIU": info_lain_dict
            , "dataSKP": riwayat_skp_arr
            , "dataSKP_col": 5
        })
        return json_format

    def dump_str_json(self,dict,index,size):
        arr = json.dumps(dict)
        if not index == size:
            arr += ","
        return arr

    def get_loc_by_key(self,parent,parent_seq,node,node_seq):
        secretary_offset_x = 400
        un_secretary_distance_x = 240
        un_secretary_offset_x = 460
        pos_x,pos_y = 0,0

        if parent_seq < 0:
            if node_seq < 0: #level1
                pos_x = node_distance_x
            else: #level2
                if node.is_secretary: #this is secretary
                    pos_x = node_distance_x + secretary_offset_x
                    pos_y = node_distance_y
                else:
                    pos_x = node_seq*node_distance_x
                    pos_y = kabid_mul_factor * node_distance_y
        else: #level3
            if not parent.is_secretary:
                pos_x = parent_seq * node_distance_x
                pos_y = (kabid_mul_factor + node_seq + 1) * node_distance_y
            else:
                pos_x = un_secretary_offset_x + node_seq*un_secretary_distance_x
                pos_y = (kabid_mul_factor - 1) * node_distance_y

        value = "{} {}".format(pos_x, pos_y)
        return value

