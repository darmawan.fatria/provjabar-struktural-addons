##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

_JENIS_PENGHARGAAN = [('Satyalancana Wira Karya','Satyalancana Wira Karya'),
                     ('Satyalancana Karya Satya','Satyalancana Karya Satya'),
                     ('Pembangunan','Pembangunan'),
                      ]
_JENIS_KEDISIPLINAN = [('ringan', 'Ringan'),
                                        ('sedang_kgb', 'Sedang (Penundaan Kenaikan Gaji Berkala)'),
                                        ('sedang_penurunan', 'Sedang (Penurunan Gaji Dan/Atau Pangkat)'),
                                        ('berat', 'Berat'),
                                        ]
_JENIS_DIKLATPIM = [('diklatpim4','Diklatpim tk.IV atau sederajat'),
                     ('diklatpim3','Diklatpim tk.III atau sederajat'),
                     ('diklatpim2','Diklatpim tk.II atau sederajat'),
                      ]
# ====================== Konfigurasi Berdasarkan Pangkat ================================= #
class konfigurasi_struktural_pangkat(osv.Model):
    _name = "konfigurasi.struktural.pangkat"
    _description ="Konfigurasi Pemetaan Berdasarkan Pangkat"
    _columns = {
        'eselon_id'     : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'golongan_id'   : fields.many2one('partner.employee.golongan', 'Golongan',required=True),
        'value'         : fields.float("Bobot Nilai",required=True ),
        'description'   : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, eselon_id,golongan_id,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('eselon_id', '=', eselon_id),('golongan_id', '=', golongan_id),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_pangkat()
# ====================== Konfigurasi Berdasarkan Usia ================================= #
class konfigurasi_struktural_usia(osv.Model):
    _name = "konfigurasi.struktural.usia"
    _description ="Konfigurasi Pemetaan Berdasarkan Usia"
    _columns = {
        'eselon_id'     : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'usia_min'    : fields.integer("Usia Minimum",required=True),
        'usia_max'    : fields.integer("UsiaMaksimum",required=True),
        'value'         : fields.float("Bobot Nilai",required=True),
        'description'   : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, eselon_id,usia,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('eselon_id', '=', eselon_id),
                                          ('usia_min', '<=', usia),
                                          ('usia_max', '>=', usia),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_usia()
# ====================== Konfigurasi Berdasarkan Masa Kerja ================================= #
class konfigurasi_struktural_masa_kerja(osv.Model):
    _name = "konfigurasi.struktural.masa.kerja"
    _description ="Konfigurasi Pemetaan Berdasarkan Masa Kerja"
    _columns = {
        'eselon_id'     : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'masa_kerja_min'    : fields.integer("Masa Kerja Minimum",required=True),
        'masa_kerja_max'    : fields.integer("Masa Kerja Maksimum",required=True),
        #'max_data_value'     : fields.boolean('Batas Maksimum Masa Kerja'),
        'value'         : fields.float("Bobot Nilai",required=True),
        'description'   : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, eselon_id,masa_kerja_thn,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('eselon_id', '=', eselon_id),
                                          ('masa_kerja_min', '<=', masa_kerja_thn),
                                          ('masa_kerja_max', '>=', masa_kerja_thn),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_masa_kerja()

# ====================== Konfigurasi Berdasarkan Tingkat Jabatan ================================= #
class konfigurasi_struktural_tingkat_jabatan(osv.Model):
    _name = "konfigurasi.struktural.tingkat.jabatan"
    _description ="Konfigurasi Pemetaan Berdasarkan Tingkat Jabatan"
    _columns = {
        'eselon_id'         : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'eselon_ids'        : fields.many2many('partner.employee.eselon', 'konf_struk_tingkat_jabatan_eselon_masa_kerja', 'eselon_id', 'config_tingkat_jabatan_id', 'Menjabat Sebagai Eselon'),
        'masa_kerja_min'    : fields.integer("Masa Kerja Minimum",required=True),
        'masa_kerja_max'    : fields.integer("Masa Kerja Maksimum",required=True),
        'value'                    : fields.float("Bobot Nilai",required=True),
        'description'              : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, eselon_id,employee_eselon_id,masa_kerja,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('eselon_id', '<=', eselon_id),
                                          ('masa_kerja_min', '<=', masa_kerja),
                                          ('masa_kerja_max', '>=', masa_kerja),
                                ],context=None)
            #print " data_ids :",len(data_ids)
            for data in self.browse(cr,uid,data_ids,context=None):
                #print employee_eselon_id," , ",data.eselon_ids
                for es in data.eselon_ids :
                    if es.id == employee_eselon_id :
                        #print "true : ",data.value
                        ret_val=data.value
        except:
            ret_val = 0

        return ret_val
konfigurasi_struktural_tingkat_jabatan()

# ====================== Konfigurasi Berdasarkan Rata-Rata Nilai SKP ================================= #
class konfigurasi_struktural_skp(osv.Model):
    _name = "konfigurasi.struktural.skp"
    _description ="Konfigurasi Pemetaan Berdasarkan Rata Rata Nilai SKP"
    _columns = {
        'nilai_skp_min'                : fields.float("Rata Rata Nilai SKP (2thn Terakhir) [Min] ",required=True),
        'nilai_skp_max'                : fields.float("Rata Rata Nilai SKP (2thn Terakhir) [Max]",required=True),
        'value'                    : fields.float("Bobot Nilai",required=True),
        'description'              : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, rata_rata_skp_2thn,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('nilai_skp_min', '<=', rata_rata_skp_2thn),
                                          ('nilai_skp_max', '>=', rata_rata_skp_2thn),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_skp()

# ====================== Konfigurasi Berdasarkan Penghargaan ================================= #
class konfigurasi_struktural_penghargaan(osv.Model):
    _name = "konfigurasi.struktural.penghargaan"
    _description ="Konfigurasi Pemetaan Berdasarkan Penghargaan"
    _columns = {
        'penghargaan_type_id': fields.many2one('penghargaan.type', 'Jenis Penghargaan',required=True),
        'lama_waktu'               : fields.integer("Lama Waktu",required=False),
        'value'                    : fields.float("Bobot Nilai",required=True),
        'accumulate'               : fields.boolean("Akumulasikan nilai"),
        'description'              : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, penghargaan_type_id,context=None):
        ret_val  =  0
        is_accumulate = False
        try :
            data_ids= self.search(cr,uid,[('penghargaan_type_id', '=', penghargaan_type_id),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value','accumulate'], context=context)
            for record in results:
                ret_val = record['value']
                is_accumulate = record['accumulate']
        except:
            ret_val = 0
        return ret_val,is_accumulate

konfigurasi_struktural_penghargaan()

# ====================== Konfigurasi Berdasarkan Kedisiplinan ================================= #
class konfigurasi_struktural_kedisiplinan(osv.Model):
    _name = "konfigurasi.struktural.kedisiplinan"
    _description ="Konfigurasi Pemetaan Berdasarkan Penghargaan"
    _columns = {
        'jenis_kedisiplinan'       : fields.selection(_JENIS_KEDISIPLINAN,'Jenis Kedisiplinan',required=True),
        'value'                    : fields.float("Bobot Nilai",required=True),
        'description'              : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, hukdis_type,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('jenis_kedisiplinan', '=', hukdis_type)
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_kedisiplinan()
# ====================== Konfigurasi Berdasarkan Pendidikan ================================= #
class konfigurasi_struktural_pendidikan(osv.Model):
    _name = "konfigurasi.struktural.pendidikan"
    _description ="Konfigurasi Pemetaan Berdasarkan Pendidikan"
    _columns = {
       'jenjang_pendidikan_ids'   : fields.one2many('konfigurasi.struktural.pendidikan.detail','parent_id', string='Jenjang Pendidikan'),
       'name'         : fields.char("Nama Konfigurasi",size=45,required=True ),
       'value'         : fields.float("Bobot Nilai",required=True ),
      'description'   : fields.text("Deskripsi"),
    }

konfigurasi_struktural_pendidikan()
class konfigurasi_struktural_pendidikan_detail(osv.Model):
    _name = "konfigurasi.struktural.pendidikan.detail"
    _description ="Konfigurasi Pemetaan Berdasarkan Pendidikan Detail"
    _columns = {
       'parent_id'   : fields.many2one('konfigurasi.struktural.pendidikan', 'Konfigurasi'),
       'jenjang_pendidikan_id'   : fields.many2one('partner.employee.study.degree', 'Jenjang Pendidikan',required=True),
       'min': fields.integer("Jumlah (min)",required=True),
       'max': fields.integer("Jumlah (max)",required=True),

    }
konfigurasi_struktural_pendidikan_detail()
# ====================== Konfigurasi Berdasarkan Diklatpim ================================= #
class konfigurasi_struktural_diklatpim(osv.Model):
    _name = "konfigurasi.struktural.diklatpim"
    _description ="Konfigurasi Pemetaan Berdasarkan Diklatpim"
    _columns = {
        'diklatpim_id': fields.many2one('diklat.type', 'Jenjang',domain="[('type','=','kepemimpinan')]" ),
        'jumlah_diklatpim_min': fields.integer("Jumlah Diklatpim (min)",required=True),
        'jumlah_diklatpim_max': fields.integer("Jumlah Diklatpim (max)",required=True),
        'eselon_id'     : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'value'         : fields.float("Bobot Nilai",required=True ),
        'description'   : fields.text("Deskripsi"),

    }
    def get_value(self, cr, uid, eselon_id,diklatpim_id,jumlah_diklatpim,context=None):
        ret_val  =  0
        try :
            domain = [('eselon_id', '=', eselon_id),
                      ('jumlah_diklatpim_min', '<=', jumlah_diklatpim),
                      ('jumlah_diklatpim_max', '>=', jumlah_diklatpim),
                     ]
            if diklatpim_id:
                domain = [('eselon_id', '=', eselon_id),
                                              ('diklatpim_id', '=', diklatpim_id),
                                              ('jumlah_diklatpim_min', '<=', jumlah_diklatpim),
                                              ('jumlah_diklatpim_max', '>=', jumlah_diklatpim),
                                    ]

            data_ids= self.search(cr,uid,domain,context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_diklatpim()
# ====================== Konfigurasi Berdasarkan Pelatihan Dan Seminar ================================= #
class konfigurasi_struktural_seminar(osv.Model):
    _name = "konfigurasi.struktural.seminar"
    _description ="Konfigurasi Pemetaan Berdasarkan Pangkat"
    _columns = {
        'eselon_id'     : fields.many2one('partner.employee.eselon', 'Eselon',required=True),
        'jumlah_seminar_min': fields.integer("Jumlah Pelatihan Atau Seminar (Min)",required=True),
        'jumlah_seminar_max': fields.integer("Jumlah Pelatihan Atau Seminar (Max)",required=True),
        'value'         : fields.float("Bobot Nilai",required=True ),
        'description'   : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, eselon_id,jumlah_seminar_dan_diklat,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('eselon_id', '=', eselon_id),
                                          ('jumlah_seminar_min', '<=', jumlah_seminar_dan_diklat),
                                          ('jumlah_seminar_max', '>=', jumlah_seminar_dan_diklat),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_seminar()
# ====================== Konfigurasi Berdasarkan Pengalaman Berkarya Pada Bidang  Pemerintahan / Keilmuan / keprofesian ================================= #
class konfigurasi_struktural_pengalaman_berkarya(osv.Model):
    _name = "konfigurasi.struktural.pengalaman.berkarya"
    _description ="Konfigurasi Jenis Pengalaman Berkarya"
    _columns = {
        'pengalaman_type_id'     : fields.many2one('pengalaman.type', 'Jenis Pengalaman',required=True),
        'kegiatan_type_id'     : fields.many2one('kegiatan.type', 'Jenis Kegiatan',required=True,domain="[('pengalaman_type_id','=',pengalaman_type_id)]"),
        'jumlah_min': fields.integer("Jumlah (Min)",required=True),
        'jumlah_max': fields.integer("Jumlah (Max)",required=True),
        'value'         : fields.float("Bobot Nilai",required=True ),
        'description'   : fields.text("Deskripsi"),
    }
    def get_value(self, cr, uid, pengalaman_type_id,kegiatan_type_id,jumlah,context=None):
        ret_val  =  0
        try :
            data_ids= self.search(cr,uid,[('pengalaman_type_id', '=', pengalaman_type_id),
                                           ('kegiatan_type_id', '=', kegiatan_type_id),
                                          ('jumlah_min', '<=', jumlah),
                                          ('jumlah_max', '>=', jumlah),
                                ],context=None)
            results = self.read(cr, uid, data_ids, ['value'], context=context)
            for record in results:
                ret_val = record['value']
        except:
            ret_val = 0
        return ret_val
konfigurasi_struktural_pengalaman_berkarya()
class konfigurasi_jenis_pengalaman_berkarya(osv.Model):
    _name = "konfigurasi.jenis.pengalaman.berkarya"
    _columns = {

        'description'   : fields.text("Deskripsi"),
    }
konfigurasi_jenis_pengalaman_berkarya()
class konfigurasi_struktural_lain(osv.Model):
    _name = "konfigurasi.struktural.lain"
    _description ="Konfigurasi Pemetaan Pengalaman Berkarya"
    _columns = {
        'eselon_id'     : fields.many2one('konfigurasi.jenis.pengalaman.berkarya', 'Kegiatan',required=True),
        'jumlah_min': fields.integer("Jumlah Partisipasi (Min)",required=True),
        'jumlah_max': fields.integer("Jumlah Partisipasi",required=True),
        'value'         : fields.float("Bobot Nilai",required=True ),
        'description'   : fields.text("Deskripsi"),
    }
konfigurasi_struktural_lain()