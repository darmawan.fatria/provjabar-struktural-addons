##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID
from openerp.osv import fields
from openerp.osv import osv

class pengajuan_pemetaan_jabatan(osv.Model):
    _name = 'pemetaan.rotasi.pegawai'
    _description = 'pemetaan rotasi pegawai'
    _columns = {
        'pemetaan_jabatan_id'	   	: fields.many2one('pemetaan.jabatan', 'Pemetaan Jabatan', required=True, select=True),
        'employee_id'               : fields.many2one('res.partner', 'Pegawai',domain=[('employee','=',True)],),
        'company_id'	   	        : fields.related('employee_id','company_id',type='many2one',relation='res.company', string='OPD', store=True,),
        'department_id'				: fields.related('employee_id','department_id',type='many2one',relation='partner.employee.department',  string='Unit Kerja', store=True),
        'golongan_id'		        : fields.related('employee_id','golongan_id',type='many2one',relation='partner.employee.golongan',  string='Pangkat/Gol.Ruang',store=True),
        'job_id'				    : fields.related('employee_id','job_id',type='many2one',relation='partner.employee.job',  string='Jabatan', store=True),
        'eselon_id'					: fields.related('employee_id','eselon_id',type='many2one',relation='partner.employee.eselon',  string='Eselon', store=True,),

        'active': fields.boolean('Active'),

    }
    _defaults = {
        'active':True,
    }
    _order = 'total_value desc'
    def deactive(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'active':False}, context=context)
pengajuan_pemetaan_jabatan()