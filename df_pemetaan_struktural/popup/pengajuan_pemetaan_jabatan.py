##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

class pengajuan_pemetaan_jabatan(osv.Model):
    _name = 'pengajuan.pemetaan.jabatan'
    _description ='Popup Pengajuan Pemetaan Jabatan'

    _columns = {
		'pemetaan_jabatan_id' : fields.many2one('pemetaan.jabatan','Pemetaan Jabatan' , required=True),
        'employee_id' : fields.many2one('res.partner','Pegawai Yang Diajukan' , required=True),
        'type'       : fields.selection([('rotasi', 'Rotasi'),('promosi', 'Promosi')], 'Jenis Usulan',required=True),
    }

    def action_pengajuan_pemetaan_jabatan_rotasi(self, cr, uid, ids, context=None):
        """  Pengajuan Pengisiaan jabatan rotasi oleh kepala dinas
        """
        result = False
        pemetaan_jabatan_pool = self.pool.get('pemetaan.jabatan')
        for pengajuan in self.browse(cr,uid,ids,context=None):
            calc_type = 'recommendation'
            result = pemetaan_jabatan_pool.do_rotation_calculate(cr,uid,pengajuan.pemetaan_jabatan_id,[pengajuan.employee_id.id],calc_type,context=None)
            pemetaan_jabatan_pool.action_rotation_remarks_recommendation_by_kepala(cr,uid,pengajuan.pemetaan_jabatan_id,pengajuan.employee_id.id,'Diajukan Rotasi Oleh Kepala Instansi',context=None)
        return result;
    def action_pengajuan_pemetaan_jabatan_promosi(self, cr, uid, ids, context=None):
        """  Pengajuan Pengisiaan jabatan promosi oleh kepala dinas
        """
        result = False
        pemetaan_jabatan_pool = self.pool.get('pemetaan.jabatan')
        for pengajuan in self.browse(cr,uid,ids,context=None):
            calc_type = 'recommendation'
            result = pemetaan_jabatan_pool.do_promotion_calculate(cr,uid,pengajuan.pemetaan_jabatan_id,[pengajuan.employee_id.id],calc_type,context=None)
            pemetaan_jabatan_pool.action_promotion_remarks_recommendation_by_kepala(cr,uid,pengajuan.pemetaan_jabatan_id,pengajuan.employee_id.id,'Diajukan Promosi Oleh Kepala Instansi',context=None)
        return result;

pengajuan_pemetaan_jabatan()