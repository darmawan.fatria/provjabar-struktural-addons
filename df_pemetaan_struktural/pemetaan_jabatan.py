##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
import openerp.addons.decimal_precision as dp
from openerp import SUPERUSER_ID
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _
#======================== Pemetaan Jabatan ====================#
_MAKS_MASA_KERJA_JABATAN = 24
class pemetaan_jabatan(osv.Model):
    _name = 'pemetaan.jabatan'
    _description = 'pemetaan jabatan'

    def get_childs_fn(self,cr,uid,parent_id,context=None):
        if not parent_id:
            return None
        child_ids = self.search(cr, uid, [('parent_id', '=', parent_id)], context=context)
        return self.browse(cr,uid,child_ids, context=context)
    def _get_childs(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for id in ids:
            child_ids = self.search(cr, uid, [('parent_id', '=', id)], context=context)
            res[id]=child_ids
        return res


    _columns = {
        'job_type'                  : fields.selection([('struktural', 'Jabatan Struktural')], 'Jenis Jabatan',required=True),
        'company_id'    		   	: fields.many2one('res.company', 'OPD', required=True, select=True),
        'department_id'				: fields.many2one('partner.employee.department', 'Unit Kerja', required=True, select=True),
        'job_id'				    : fields.many2one('partner.employee.job', 'Jabatan', required=False),
        'eselon_id'					: fields.many2one('partner.employee.eselon', 'Eselon', required=False,),
        'name'                      : fields.char('Nama Jabatan',size=120, required=True, select=True),
        'code'                      : fields.char('Kode Jabatan',size=25, required=True,),
        'parent_id'     			: fields.many2one('pemetaan.jabatan', 'Parent',  ),
        'level'  					: fields.integer('Level Jabatan',required=True),
        'golongan_id'		        : fields.many2one('partner.employee.golongan', 'Pangkat/Gol.Ruang',),
        'employee_id'   : fields.many2one('res.partner', 'Pegawai',domain=[('employee','=',True)],),
        'child_ids': fields.function(_get_childs, method=True,readonly=True , store=False,
                                         type="one2many",relation="pemetaan.jabatan",string='Masa Kerja Tahun'),
        #pembobotan value
        'rotasi_ids': fields.one2many('pemetaan.rotasi.pegawai', 'pemetaan_jabatan_id', 'Daftar Rekomendasi Dalam Rotasi',domain=[('type','=','system')],limit=15),
        'promosi_ids': fields.one2many('pemetaan.promosi.pegawai', 'pemetaan_jabatan_id', 'Daftar Rekomendasi Dalam Promosi',domain=[('type','=','system')],limit=25),
        'rekomendasi_rotasi_ids': fields.one2many('pemetaan.rotasi.pegawai', 'pemetaan_jabatan_id', 'Daftar Rekomendasi Rotasi Kepala Instansi',domain=[('type','=','recommendation')],limit=1),
        'rekomendasi_promosi_ids': fields.one2many('pemetaan.promosi.pegawai', 'pemetaan_jabatan_id', 'Daftar Rekomendasi Promosi Kepala Instansi',domain=[('type','=','recommendation')],limit=3),

        'sequence'  					: fields.integer('Sequence',required=False),
        #edit adri 28/7/2016
        'is_secretary'  				: fields.boolean('Jabatan Sekretaris',required=False)

    }
    _order = 'level,sequence'

    def action_reset_rotation(self, cr, uid, ids,context=None):
        rotasi_pool = self.pool.get('pemetaan.rotasi.pegawai')
        all_active_rotation_ids=rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                          ('type', '=', 'system'),
                                          ('active', '=', True)],context=None)
        rotasi_pool.deactive(cr,uid,all_active_rotation_ids,context=None)
        return True
    def action_reset_recommendation_rotation(self, cr, uid, ids,context=None):
        rotasi_pool = self.pool.get('pemetaan.rotasi.pegawai')
        all_active_rotation_ids=rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                          ('type', '=', 'recommendation'),
                                          ('active', '=', True)],context=None)
        rotasi_pool.deactive(cr,uid,all_active_rotation_ids,context=None)
        return True

    def action_rotation(self, cr, uid, ids,context=None):
        employee_pool = self.pool.get('res.partner')

        for pj in self.browse(cr,uid,ids, context=None):
            employee_ids = employee_pool.search(cr,uid, [('eselon_id', '=', pj.eselon_id.id),
                                          ('employee', '=', True),
                                          ('active', '=', True)],context=None)
            print "len > ",len(employee_ids)
            if len(employee_ids) > 20:
                #optimization
                employee_ids = self.filter_by_masa_kerja(cr,uid,employee_ids,context=None)

            self.do_rotation_calculate(cr,uid,pj,employee_ids,'system',context=None)



        return True
    def do_rotation_calculate(self,cr,uid,pj,employee_ids,calc_type,context=None):
            employee_pool = self.pool.get('res.partner')
            rotasi_pool = self.pool.get('pemetaan.rotasi.pegawai')
            config_pendidikan_pool = self.pool.get('konfigurasi.struktural.pendidikan')
            pendidikan_all_ids = config_pendidikan_pool.search(cr,SUPERUSER_ID,[],context=None)
            pendidikan_lookup_list = config_pendidikan_pool.browse(cr,SUPERUSER_ID,pendidikan_all_ids,context=None)
            config_degree_pool = self.pool.get('partner.employee.study.degree')
            degree_all_ids = config_degree_pool.search(cr,SUPERUSER_ID,[],context=None)
            degree_list = config_degree_pool.browse(cr,SUPERUSER_ID,degree_all_ids,context=None)
            print "total pegawai eselon  ",pj.eselon_id.id, " : ",len(employee_ids)
            if calc_type=='system' :
                latest_rotation_ids=rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', '=', pj.id),
                                                               ('type', '=', calc_type),
                                                                ('active', '=', True)],context=None)
                rotasi_pool.deactive(cr,uid,latest_rotation_ids,context=None)

            list_data = []
            for employee in employee_pool.browse(cr,uid,employee_ids,context=None):
                if employee.masa_kerja_jabatan <_MAKS_MASA_KERJA_JABATAN:
                    continue;
                employee_pts_dict = {}
                employee_pts_dict['pemetaan_jabatan_id'] = pj.id
                employee_pts_dict['employee_id'] = employee.id
                employee_pts_dict['golongan_value'] = 0
                employee_pts_dict['usia_value'] = 0
                employee_pts_dict['masa_kerja_value'] = 0
                employee_pts_dict['skp_value'] = 0
                employee_pts_dict['kedisiplinan_value']=0
                employee_pts_dict['pelatihan_struktural_value'] = 0
                employee_pts_dict['penghargaan_value'] = 0
                employee_pts_dict['pendidikan_value'] = 0
                employee_pts_dict['seminar_value'] = 0
                employee_pts_dict['tingkat_jabatan_value'] = 0
                employee_pts_dict['pengalaman_value'] = 0
                total_value = 0
                if employee.golongan_id:
                    pangkat_pts= self.get_pangkat_point(cr,uid,employee)
                    employee_pts_dict['golongan_value'] = pangkat_pts
                    total_value+=pangkat_pts
                if employee:
                    usia_pts= 0##self.get_usia_point(cr,uid,employee)
                    employee_pts_dict['usia_value'] = usia_pts
                    total_value+=usia_pts
                if employee.masa_kerja_thn > 0:
                    masa_kerja_pts= self.get_masa_kerja_point(cr,uid,employee)
                    employee_pts_dict['masa_kerja_value'] = masa_kerja_pts
                    total_value+=masa_kerja_pts
                    #tingkat jabatan
                    tingkat_jabatan_pts = self.get_tingkat_jabatan_point(cr,uid,employee)
                    employee_pts_dict['tingkat_jabatan_value'] = tingkat_jabatan_pts
                    total_value+=tingkat_jabatan_pts
                if employee.nilai_rata2_skp_2thn > 0:
                    rata2_skp_pts= self.get_skp_point(cr,uid,employee)
                    employee_pts_dict['skp_value'] = rata2_skp_pts
                    total_value+=rata2_skp_pts
                #pernah hukuman disiplin
                if employee.hukuman_disiplin_id_history :
                    hukdis_pts = self.get_hukuman_disiplin_point(cr,uid,employee)
                    employee_pts_dict['kedisiplinan_value'] = hukdis_pts
                    total_value+=hukdis_pts
                #diklatpim
                if employee:
                    diklatpim_pts = self.get_diklatpim_point(cr,uid,employee)
                    employee_pts_dict['pelatihan_struktural_value'] = diklatpim_pts
                    total_value+=diklatpim_pts
                #penghargaan
                if employee.penghargaan_ids:
                    penghargaan_pts = self.get_penghargaan_point(cr,uid,employee)
                    employee_pts_dict['penghargaan_value'] = penghargaan_pts
                    total_value+=penghargaan_pts
                #pendidikan
                if employee.jenjang_pendidikan_id_history:
                    pendidikan_pts = self.get_pendidikan_point(cr,uid,employee,pendidikan_lookup_list,degree_list)
                    employee_pts_dict['pendidikan_value'] = pendidikan_pts
                    total_value+=pendidikan_pts
                #diklat_seminar
                if employee.jenjang_pendidikan_id_history:
                    diklat_seminar_pts = self.get_seminar_point(cr,uid,employee)
                    employee_pts_dict['seminar_value'] = diklat_seminar_pts
                    total_value+=diklat_seminar_pts
                #pengalaman berkarya
                if employee.pengalaman_berkarya_ids:
                    pengalaman_berkarya_pts = self.get_pengalaman_berkarya_point(cr,uid,employee)
                    employee_pts_dict['pengalaman_value'] = pengalaman_berkarya_pts
                    total_value+=pengalaman_berkarya_pts


                employee_pts_dict['total_value'] = total_value
                list_data.append(employee_pts_dict)


            for employee_data in list_data:
                insert_data =  {}
                insert_data.update({
                           'type':calc_type,
                           'pemetaan_jabatan_id':employee_data['pemetaan_jabatan_id'],
                           'employee_id':employee_data['employee_id'],

                           'golongan_value':employee_data['golongan_value'],
                           'masa_kerja_value':employee_data['masa_kerja_value'],
                           'usia_value':employee_data['usia_value'],
                           'skp_value':employee_data['skp_value'],
                           'kedisiplinan_value':employee_data['kedisiplinan_value'],
                           'pelatihan_struktural_value':employee_data['pelatihan_struktural_value'],
                           'penghargaan_value' : employee_data['penghargaan_value'] ,
                           'pendidikan_value' : employee_data['pendidikan_value'] ,
                           'seminar_value' : employee_data['seminar_value'] ,
                           'tingkat_jabatan_value' : employee_data['tingkat_jabatan_value'],
                           'pengalaman_value' : employee_data['pengalaman_value'],
                           'total_value':employee_data['total_value'],
                 })
                new_id = rotasi_pool.create(cr, uid, insert_data,context=None)
                #update_total_value_pegawai_sql
                self.update_total_value_pegawai_sql(cr,uid,employee_data['employee_id'],employee_data['total_value'])
            return True

    def action_rotation_remarks_recommendation_by_kepala(self, cr, uid, pj,employee_id,remarks,context=None):
        rotasi_pool = self.pool.get('pemetaan.rotasi.pegawai')

        remarks_ids=rotasi_pool.search(cr,uid,[('pemetaan_jabatan_id', '=', pj.id),
                                                            ('employee_id', '=', employee_id),
                                          ('type', '=', 'system'),
                                          ('active', '=', True)],context=None)
        rotasi_pool.set_remarks(cr,uid,remarks_ids,remarks,context=None)
        return True

    def action_reset_promotion(self, cr, uid, ids,context=None):
        promosi_pool = self.pool.get('pemetaan.promosi.pegawai')
        all_active_rotation_ids=promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                                            ('type', '=', 'system'),
                                          ('active', '=', True)],context=None)
        promosi_pool.deactive(cr,uid,all_active_rotation_ids,context=None)
        return True
    def action_reset_recommendation_promotion(self, cr, uid, ids,context=None):
        promosi_pool = self.pool.get('pemetaan.promosi.pegawai')
        all_active_rotation_ids=promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', 'in', ids),
                                                            ('type', '=', 'recommendation'),
                                          ('active', '=', True)],context=None)
        promosi_pool.deactive(cr,uid,all_active_rotation_ids,context=None)
        return True
    def action_promotion(self, cr, uid, ids,context=None):
        print "action_promotion...."
        employee_pool = self.pool.get('res.partner')

        eselon_pool = self.pool.get('partner.employee.eselon')


        for pj in self.browse(cr,uid,ids, context=None):
            es_ids = eselon_pool.search(cr,SUPERUSER_ID,[('sequence', '=', (pj.eselon_id.sequence+1)) ],context=None)
            print (pj.eselon_id.sequence+1),"level dibawah :",es_ids
            if not es_ids:
                employee_ids = employee_pool.search(cr,uid, [('eselon_id', '=', pj.eselon_id.id),
                                              ('employee', '=', True),
                                              ('active', '=', True)],context=None)
            else:
                employee_ids = employee_pool.search(cr,uid, [('eselon_id', 'in', es_ids),
                                              ('employee', '=', True),
                                              ('active', '=', True)],context=None)
                if len(employee_ids) == 0 :
                    employee_ids = employee_pool.search(cr,uid, [('job_type', '!=', 'struktural'),
                                              ('employee', '=', True),
                                              ('active', '=', True)],context=None)
            print "len > ",len(employee_ids)
            if len(employee_ids) > 20:
                #optimization
                employee_ids = self.filter_by_masa_kerja(cr,uid,employee_ids,context=None)

            self.do_promotion_calculate(cr,uid,pj,employee_ids,'system')

        return True
    def filter_by_masa_kerja(self,cr,uid,employee_ids,context=None):

        masa_kerja_jab_sql = """
                    select p.id,p.nip,p.name,
                    ( (date_part('year',current_date::date) - date_part('year',h.date::date)) * 12)
                    + (date_part('month',current_date::date) - date_part('month',h.date::date) ) masa_kerja_jabatan_bln
                 from res_partner p
                join (
                    select h.partner_id,
                    max(h.date) date
                    from partner_employee_job_history h
                    group by h.partner_id
                ) h on h.partner_id = p.id
                where employee
        """
        str_employee_ids=''
        for emp_id in employee_ids:
            str_employee_ids+=str(emp_id)+','
        masa_kerja_jab_sql += ' and id in ('+str_employee_ids+'0)'
        main_sql = """
        select * from ( """+masa_kerja_jab_sql+"""
        ) emp where emp.masa_kerja_jabatan_bln >"""+str(_MAKS_MASA_KERJA_JABATAN)
        list_data = []
        cr.execute(main_sql)
        result = cr.fetchall()
        for  employee_id,employee_nip,employee_name,masa_kerja_bln in result:
            list_data.append(employee_id)
        print "filtered employee ids ",len(list_data)
        return list_data

    def do_promotion_calculate(self,cr,uid,pj,employee_ids,calc_type,context=None):
            promosi_pool = self.pool.get('pemetaan.promosi.pegawai')
            employee_pool = self.pool.get('res.partner')
            config_pendidikan_pool = self.pool.get('konfigurasi.struktural.pendidikan')
            pendidikan_all_ids = config_pendidikan_pool.search(cr,SUPERUSER_ID,[],context=None)
            config_degree_pool = self.pool.get('partner.employee.study.degree')
            degree_all_ids = config_degree_pool.search(cr,SUPERUSER_ID,[],context=None)
            pendidikan_lookup_list = config_pendidikan_pool.browse(cr,SUPERUSER_ID,pendidikan_all_ids,context=None)
            degree_list = config_degree_pool.browse(cr,SUPERUSER_ID,degree_all_ids,context=None)
            print "total pegawai eselon  ",pj.eselon_id.name, " : ",len(employee_ids)
            if calc_type =='system' :
                latest_rotation_ids=promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', '=', pj.id),
                                                                ('type', '=', calc_type),
                                              ('active', '=', True)],context=None)
                promosi_pool.deactive(cr,uid,latest_rotation_ids,context=None)


            list_data = []
            for employee in employee_pool.browse(cr,uid,employee_ids,context=None):
                if employee.masa_kerja_jabatan <_MAKS_MASA_KERJA_JABATAN:
                    continue;
                employee_pts_dict = {}
                employee_pts_dict['pemetaan_jabatan_id'] = pj.id
                employee_pts_dict['employee_id'] = employee.id
                employee_pts_dict['golongan_value'] = 0
                employee_pts_dict['usia_value'] = 0
                employee_pts_dict['masa_kerja_value'] = 0
                employee_pts_dict['skp_value'] = 0
                employee_pts_dict['kedisiplinan_value']=0
                employee_pts_dict['pelatihan_struktural_value'] = 0
                employee_pts_dict['penghargaan_value'] = 0
                employee_pts_dict['pendidikan_value'] = 0
                employee_pts_dict['seminar_value'] = 0
                employee_pts_dict['tingkat_jabatan_value'] = 0
                employee_pts_dict['pengalaman_value'] = 0
                total_value = 0
                if employee.golongan_id:
                    pangkat_pts= self.get_pangkat_point(cr,uid,employee)
                    employee_pts_dict['golongan_value'] = pangkat_pts
                    total_value+=pangkat_pts
                if employee:
                    usia_pts=0 #self.get_usia_point(cr,uid,employee)
                    employee_pts_dict['usia_value'] = usia_pts
                    total_value+=usia_pts
                #masa kerja
                if employee.masa_kerja_thn > 0:
                    masa_kerja_pts= self.get_masa_kerja_point(cr,uid,employee)
                    employee_pts_dict['masa_kerja_value'] = masa_kerja_pts
                    total_value+=masa_kerja_pts

                if employee.nilai_rata2_skp_2thn > 0:
                    rata2_skp_pts= self.get_skp_point(cr,uid,employee)
                    employee_pts_dict['skp_value'] = rata2_skp_pts
                    total_value+=rata2_skp_pts
                #pernah hukuman disiplin
                if employee.hukuman_disiplin_id_history :
                    hukdis_pts = self.get_hukuman_disiplin_point(cr,uid,employee)
                    employee_pts_dict['kedisiplinan_value'] = hukdis_pts
                    total_value+=hukdis_pts
                #diklatpim
                if employee:
                    diklatpim_pts = self.get_diklatpim_point(cr,uid,employee)
                    employee_pts_dict['pelatihan_struktural_value'] = diklatpim_pts
                    total_value+=diklatpim_pts
                #penghargaan
                if employee.penghargaan_ids:
                    penghargaan_pts = self.get_penghargaan_point(cr,uid,employee)
                    employee_pts_dict['penghargaan_value'] = penghargaan_pts
                    total_value+=penghargaan_pts
                #pendidikan
                if employee.jenjang_pendidikan_id_history:
                    pendidikan_pts = self.get_pendidikan_point(cr,uid,employee,pendidikan_lookup_list,degree_list)
                    employee_pts_dict['pendidikan_value'] = pendidikan_pts
                    total_value+=pendidikan_pts
                #diklat_seminar
                if employee.jenjang_pendidikan_id_history:
                    diklat_seminar_pts = self.get_seminar_point(cr,uid,employee)
                    employee_pts_dict['seminar_value'] = diklat_seminar_pts
                    total_value+=diklat_seminar_pts
                #pengalaman berkarya
                if employee.pengalaman_berkarya_ids:
                    pengalaman_berkarya_pts = self.get_pengalaman_berkarya_point(cr,uid,employee)
                    employee_pts_dict['pengalaman_value'] = pengalaman_berkarya_pts
                    total_value+=pengalaman_berkarya_pts


                employee_pts_dict['total_value'] = total_value
                list_data.append(employee_pts_dict)


            for employee_data in list_data:
                insert_data =  {}
                insert_data.update({
                           'type':calc_type,
                           'pemetaan_jabatan_id':employee_data['pemetaan_jabatan_id'],
                           'employee_id':employee_data['employee_id'],

                           'golongan_value':employee_data['golongan_value'],
                           'masa_kerja_value':employee_data['masa_kerja_value'],
                           'usia_value':employee_data['usia_value'],
                           'skp_value':employee_data['skp_value'],
                           'kedisiplinan_value':employee_data['kedisiplinan_value'],
                           'pelatihan_struktural_value':employee_data['pelatihan_struktural_value'],
                           'penghargaan_value' : employee_data['penghargaan_value'] ,
                           'pendidikan_value' : employee_data['pendidikan_value'] ,
                           'seminar_value' : employee_data['seminar_value'] ,
                           'tingkat_jabatan_value' : employee_data['tingkat_jabatan_value'],
                           'pengalaman_value':employee_data['pengalaman_value'],
                           'total_value':employee_data['total_value'],
                 })
                new_id = promosi_pool.create(cr, uid, insert_data,context=None)
                self.update_total_value_pegawai_sql(cr,uid,employee_data['employee_id'],employee_data['total_value'])
            return True
    def action_promotion_remarks_recommendation_by_kepala(self, cr, uid, pj,employee_id,remarks,context=None):
        promosi_pool = self.pool.get('pemetaan.promosi.pegawai')

        remarks_ids=promosi_pool.search(cr,uid,[('pemetaan_jabatan_id', '=', pj.id),
                                                            ('employee_id', '=', employee_id),
                                          ('type', '=', 'system'),
                                          ('active', '=', True)],context=None)
        promosi_pool.set_remarks(cr,uid,remarks_ids,remarks,context=None)
        return True
    def get_pangkat_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.pangkat')
        return config_pool.get_value(cr,uid,employee.eselon_id.id, employee.golongan_id.id)
    def get_usia_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.usia')
        return config_pool.get_value(cr,uid,employee.eselon_id.id, employee.usia)
    def get_masa_kerja_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.masa.kerja')
        return config_pool.get_value(cr,uid,employee.eselon_id.id, employee.masa_kerja_thn)
    def get_skp_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.skp')
        return config_pool.get_value(cr,uid,employee.nilai_rata2_skp_2thn)
    def get_hukuman_disiplin_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.kedisiplinan')
        hukdis_pts = 0
        for hukdis in employee.hukuman_disiplin_id_history:
            val=config_pool.get_value(cr,uid,hukdis.name)
            hukdis_pts+=val
            #if val<hukdis_pts:
            #    hukdis_pts=val
        return hukdis_pts
    def get_diklatpim_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.diklatpim')
        total_point = 0
        for diklatpim in employee.diklat_kepemimpinan_id_history:
            val=config_pool.get_value(cr,uid,employee.eselon_id.id,diklatpim.type_id.id,1)
            if val>total_point:
                total_point=val
        if total_point == 0:
                val=config_pool.get_value(cr,uid,employee.eselon_id.id,None,0)
                if val>total_point:
                    total_point=val

        return total_point
    def get_penghargaan_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.penghargaan')
        total_point = 0
        for penghargaan in employee.penghargaan_ids:
            val,is_accumulate=config_pool.get_value(cr,uid,penghargaan.penghargaan_type_id.id)
            if not is_accumulate:
                if val>total_point:
                    total_point=val
            else :
                total_point+=val

        return total_point
    def get_pendidikan_point(self,cr,uid,employee,lookup_list,degree_list,context=None):

        total_point = 0
        degree_data_list= []
        #print "pendidikan lookup cnt : ",len(lookup_list)
        #print "degree lookup cnt : ",len(degree_list)
        for degree in degree_list:
            degree_count=0
            employee_by_degree = {}
            for pendidikan in employee.jenjang_pendidikan_id_history:
                emp_degree = pendidikan.jenjang_pendidikan_id_history.degree_id
                #print degree.name," = ", emp_degree.name
                if degree.id == emp_degree.id:
                    degree_count+=1
            if degree_count >0:
                employee_by_degree['employee_id'] = employee.id
                employee_by_degree['degree_id'] = degree.id
                employee_by_degree['degree_count'] = degree_count
                degree_data_list.append(employee_by_degree)

        #print employee.name," have ->  ",degree_data_list
        if degree_data_list:
            for lookup in lookup_list:
                found_list = [];
                for lookup_detail in lookup.jenjang_pendidikan_ids:
                    #print lookup_detail.jenjang_pendidikan_id.id,".",lookup_detail.jenjang_pendidikan_id.name
                    counting = True
                    for degree_data in degree_data_list:
                        if lookup_detail.jenjang_pendidikan_id.id == degree_data['degree_id'] and degree_data['degree_count'] >0 :
                            counting=False #jika ada yang sama ,
                            if lookup_detail.min <= degree_data['degree_count'] and lookup_detail.max >= degree_data['degree_count']:
                                found_list.append(True)
                            else :
                               found_list.append(False)
                    if counting:
                        found_list.append(False)
                if found_list: # cek all list
                    found_cnt=0
                    for found in found_list:
                        if found:
                            found_cnt+=1

                    if found_cnt == len(found_list) :
                        val=lookup.value;
                        if val > total_point:
                            total_point=val

        return total_point
    def get_seminar_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.seminar')
        total_point = 0
        total_diklat_and_seminar=0;
        if employee.diklat_fungsional_id_history:
            total_diklat_and_seminar += len(employee.diklat_fungsional_id_history)
        if employee.diklat_teknik_id_history:
            total_diklat_and_seminar += len(employee.diklat_teknik_id_history)
        if employee.penataran_id_history:
            total_diklat_and_seminar += len(employee.penataran_id_history)
        if total_diklat_and_seminar > 0:
            val=config_pool.get_value(cr,uid,employee.eselon_id.id,total_diklat_and_seminar)
            if val>total_point:
                total_point=val
        return total_point
    def get_tingkat_jabatan_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.tingkat.jabatan')
        total_point = 0

        for jabatan in employee.job_id_history:
            val=config_pool.get_value(cr,uid,employee.eselon_id.id,jabatan.eselon_id.id,jabatan.masa_kerja_thn_job_history)

            if val>total_point:
                total_point=val

        return total_point

    def get_pengalaman_berkarya_point(self,cr,uid,employee,context=None):
        config_pool = self.pool.get('konfigurasi.struktural.pengalaman.berkarya')
        total_point = 0
        list_pengalaman_group = self.pool.get('res.partner').get_pengalaman_berkarya_group(cr,uid,employee.id,context=None)
        for pengalaman in list_pengalaman_group:
            val=config_pool.get_value(cr,uid,pengalaman['pengalaman_type_id'],pengalaman['kegiatan_type_id'],pengalaman['cnt'])
            total_point+=val

        return total_point
    #pengajuan oleh kepala instansi
    def action_propose_rotation(self, cr, uid, ids,context=None):
        print " action_propose_rotation "
        if not ids: return []
        employee_pool = self.pool.get('res.partner')
        user_pool = self.pool.get('res.users')
        user_data = user_pool.browse(cr,uid,uid,context=None)
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_pemetaan_struktural', 'action_pengajuan_pemetaan_jabatan_rotasi_popup_form_view')
        param_employee_ids = employee_pool.search(cr,uid, [('company_id','=',user_data.company_id and user_data.company_id.id),
                                              ('employee', '=', True),
                                              ('active', '=', True)],context=None)

        pj_obj = self.browse(cr, uid, ids[0], context=context)
        return {
				'name':_("Pilih Pegawai Yang Akan Rotasi"),
				'view_mode': 'form',
				'view_id': view_id,
				'view_type': 'form',
				'res_model': 'pengajuan.pemetaan.jabatan',
				'type': 'ir.actions.act_window',
				'nodestroy': True,
				'target': 'new',
				'domain': '[]',
				'context': {
					'param_employee_ids':param_employee_ids,
				    'default_pemetaan_jabatan_id':pj_obj.id,
                    'default_type':'rotasi',
				}
			}
    def action_propose_promotion(self, cr, uid, ids,context=None):
        print " action_propose_promotion "
        if not ids: return []
        employee_pool = self.pool.get('res.partner')
        user_pool = self.pool.get('res.users')
        user_data = user_pool.browse(cr,uid,uid,context=None)
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_pemetaan_struktural', 'action_pengajuan_pemetaan_jabatan_rotasi_popup_form_view')
        param_employee_ids = employee_pool.search(cr,uid, [('company_id','=',user_data.company_id and user_data.company_id.id),
                                              ('employee', '=', True),
                                              ('active', '=', True)],context=None)

        pj_obj = self.browse(cr, uid, ids[0], context=context)
        return {
				'name':_("Pilih Pegawai Yang Akan Promosi"),
				'view_mode': 'form',
				'view_id': view_id,
				'view_type': 'form',
				'res_model': 'pengajuan.pemetaan.jabatan',
				'type': 'ir.actions.act_window',
				'nodestroy': True,
				'target': 'new',
				'domain': '[]',
				'context': {
					'param_employee_ids':param_employee_ids,
				    'default_pemetaan_jabatan_id':pj_obj.id,
                    'default_type':'promosi',
				}
			}
    def update_total_value_pegawai_sql(self,cr,uid,employee_id,total_value):
        update_query = """ UPDATE RES_PARTNER
                        SET nilai_rekomendasi_jabatan_struktural = %s
                        where id = %s
                                """
        cr.execute(update_query, (total_value,employee_id))
        return True;

pemetaan_jabatan()

class pemetaan_rotasi_pegawai(osv.Model):
    _name = 'pemetaan.rotasi.pegawai'
    _description = 'pemetaan rotasi pegawai'
    _columns = {
        'pemetaan_jabatan_id'	   	: fields.many2one('pemetaan.jabatan', 'Pemetaan Jabatan', required=True, select=True),
        'employee_id'               : fields.many2one('res.partner', 'Pegawai',domain=[('employee','=',True)],),
        'company_id'	   	        : fields.related('employee_id','company_id',type='many2one',relation='res.company', string='OPD', store=True,),
        'department_id'				: fields.related('employee_id','department_id',type='many2one',relation='partner.employee.department',  string='Unit Kerja', store=True),
        'golongan_id'		        : fields.related('employee_id','golongan_id',type='many2one',relation='partner.employee.golongan',  string='Pangkat/Gol.Ruang',store=True),
        'job_id'				    : fields.related('employee_id','job_id',type='many2one',relation='partner.employee.job',  string='Jabatan', store=True),
        'eselon_id'					: fields.related('employee_id','eselon_id',type='many2one',relation='partner.employee.eselon',  string='Eselon', store=True,),
        'masa_kerja_jabatan'	: fields.related('employee_id','masa_kerja_jabatan',type='integer', string='Masa Kerja Jabatan', store=False,),
        'masa_kerja_jabatan_thn'	: fields.related('employee_id','masa_kerja_jabatan_thn',type='integer', string='Masa Kerja Jabatan', store=False,),
        'masa_kerja_jabatan_bln'	: fields.related('employee_id','masa_kerja_jabatan_bln',type='integer', string='Masa Kerja Jabatan', store=False,),

        #aspek penilaian
        'golongan_value'  			: fields.float('Bobot Nilai Pangkat'),
        'usia_value'  			    : fields.float('Bobot Nilai Usia'),
        'masa_kerja_value'  		: fields.float('Bobot Nilai Masa Kerja'),
        'tingkat_jabatan_value'  	: fields.float('Bobot Nilai Tingkat Jabatan'),
        'skp_value'  			    : fields.float('Bobot Nilai Rata-Rata SKP/DP3 (2thn)'),
        'penghargaan_value'  		: fields.float('Bobot Nilai Penghargaan'),
        'kedisiplinan_value'  		: fields.float('Bobot Nilai Kedisiplinan'),
        'pendidikan_value'  		: fields.float('Bobot Nilai Pendidikan'),
        'pelatihan_struktural_value': fields.float('Bobot Nilai Pendidikan Dan Pelatihan Struktural'),
        'seminar_value'  			: fields.float('Bobot Nilai Pelatihan Dan Seminar'),
        'pengalaman_value'  		: fields.float('Bobot Nilai Pengalaman Berkarya Pada Bidang  Pemerintahan / Keilmuan / keprofesian'),
        'total_value'               : fields.float('Total'),
        'type'                      : fields.selection([('system', 'Sistem'),('recommendation', 'Hasil Rekomendasi Kepala Instansi')], 'Jenis',required=True),
        'remarks'                   : fields.text('Catatan'),
        'active': fields.boolean('Active'),

    }
    _defaults = {
        'active':True,
        'type':'system',
    }
    _order = 'total_value desc'
    def deactive(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'active':False}, context=context)
    def set_remarks(self, cr, uid, ids, remarks,context=None):
        return self.write(cr, uid, ids, {'remarks':remarks,}, context=context)
pemetaan_rotasi_pegawai()
class pemetaan_promosi_pegawai(osv.Model):
    _name = 'pemetaan.promosi.pegawai'
    _description = 'pemetaan promosi pegawai'
    _columns = {
        'pemetaan_jabatan_id'	   	: fields.many2one('pemetaan.jabatan', 'Pemetaan Jabatan', required=True, select=True),
        'employee_id'               : fields.many2one('res.partner', 'Pegawai',domain=[('employee','=',True)],),
        'company_id'	   	        : fields.related('employee_id','company_id',type='many2one',relation='res.company', string='OPD', store=True,),
        'department_id'				: fields.related('employee_id','department_id',type='many2one',relation='partner.employee.department',  string='Unit Kerja', store=True),
        'golongan_id'		        : fields.related('employee_id','golongan_id',type='many2one',relation='partner.employee.golongan',  string='Pangkat/Gol.Ruang',store=True),
        'job_id'				    : fields.related('employee_id','job_id',type='many2one',relation='partner.employee.job',  string='Jabatan', store=True),
        'eselon_id'					: fields.related('employee_id','eselon_id',type='many2one',relation='partner.employee.eselon',  string='Eselon', store=True,),
        'masa_kerja_jabatan'	: fields.related('employee_id','masa_kerja_jabatan',type='integer', string='Masa Kerja Jabatan', store=False,),
        'masa_kerja_jabatan_thn'	: fields.related('employee_id','masa_kerja_jabatan_thn',type='integer', string='Masa Kerja Jabatan', store=False,),
        'masa_kerja_jabatan_bln'	: fields.related('employee_id','masa_kerja_jabatan_bln',type='integer', string='Masa Kerja Jabatan', store=False,),

        #aspek penilaian
        'golongan_value'  			: fields.float('Bobot Nilai Pangkat'),
        'usia_value'  			    : fields.float('Bobot Nilai Usia'),
        'masa_kerja_value'  		: fields.float('Bobot Nilai Masa Kerja'),
        'tingkat_jabatan_value'  	: fields.float('Bobot Nilai Tingkat Jabatan'),
        'skp_value'  			    : fields.float('Bobot Nilai Rata-Rata SKP/DP3 (2thn)'),
        'penghargaan_value'  		: fields.float('Bobot Nilai Penghargaan'),
        'kedisiplinan_value'  		: fields.float('Bobot Nilai Kedisiplinan'),
        'pendidikan_value'  		: fields.float('Bobot Nilai Pendidikan'),
        'pelatihan_struktural_value': fields.float('Bobot Nilai Pendidikan Dan Pelatihan Struktural'),
        'seminar_value'  			: fields.float('Bobot Nilai Pelatihan Dan Seminar'),
        'pengalaman_value'  		: fields.float('Bobot Nilai Pengalaman Berkarya Pada Bidang  Pemerintahan / Keilmuan / keprofesian'),
        'total_value'               : fields.float('Total'),
        'type'                      : fields.selection([('system', 'Sistem'),('recommendation', 'Hasil Rekomendasi Kepala Instansi')], 'Jenis',required=True),
        'remarks'                   : fields.text('Catatan'),
        'active': fields.boolean('Active'),

    }
    _defaults = {
        'active':True,
        'type':'system',
    }
    _order = 'total_value desc'
    def deactive(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'active':False}, context=context)
    def set_remarks(self, cr, uid, ids, remarks,context=None):
        return self.write(cr, uid, ids, {'remarks':remarks,}, context=context)
pemetaan_promosi_pegawai()